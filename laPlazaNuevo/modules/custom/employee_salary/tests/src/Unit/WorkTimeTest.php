<?php
namespace Drupal\Tests\employee_salary\Unit;

use Drupal\Tests\UnitTestCase;
use SebastianBergmann\Environment\Console;
use Drupal\employee_salary\Model\WorkTime;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @coversDefaultClass \Drupal\employee_salary\Model\WorkTime
 * @group employee_salary
 */
class WorkTimeTest extends UnitTestCase
{

    protected $workTime;

    protected function setUp()
    {
        parent::setUp();
        
        $this->workTime = new WorkTime();
        
        $this->workTime->setEmployee(1);
        
        // 2018-01-17 07:42:57
        $this->workTime->setIngressTime(1516192977);
        
        // 2018-01-17 08:17:34
        $this->workTime->setExitTime(1516195054);
        
    }

    public function testCalculateDiurnalTime()
    {
        $dirunalTime = $this->workTime->calculateDiurnalTime();
        
        $hours = $dirunalTime->h;
        
        assertTrue($hours == 0);
        
    }
}


<?php
namespace Drupal\employee_salary\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\employee_salary\Model\EmployeeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmployeeTypeForm extends FormBase
{
    /**
     *
     * @var \Drupal\employee_salary\Model\EmployeeType
     */
    protected $employeeType;

    public function __construct(EmployeeType $employeeType)
    {
        $this->employeeType = $employeeType;
    }

    public static function create(ContainerInterface $container)
    {
        return new static($container->get('employee_salary.employee_type'));
    }

    protected function getEditableConfigNames()
    {
        return [
            'employee_salary.config'
        ];
    }

    public function getFormId()
    {
        return 'employee_type_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $id = null)
    {
        
        $this->employeeType->setId($id);
        $this->employeeType->query();
        
        $form['EmployeeTypeName'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Name '),
            '#placeholder' => t('Employee Type Name'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employeeType->getName()
        ];
        
        $form['EmployeeTypeDescription'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Description '),
            '#placeholder' => t('Employee Type Description'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employeeType->getDescription()
        ];
        
        $form['EmployeeTypeSalary'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Salary '),
            '#placeholder' => t('Employee Type Salary'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employeeType->getSalary()
        ];
        
        $form['EmployeeTypeHourValue'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Hour Value '),
            '#placeholder' => t('Employee Type Hour Value'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employeeType->getHourValue()
        ];
        
        $form['EmployeeTypeTramsportationSubsidy'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Transportation Subsidy '),
            '#placeholder' => t('Employee Type Transportation Subsidy'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employeeType->getTransportSubsidy()
        ];
        
        $form['EmployeeTypeSocialSecuriity'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Social Security '),
            '#placeholder' => t('Employee Type Social Security Apport'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employeeType->getSocialSecurity()
        ];
        
        $form['EmployeeTypeNumberHours'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Number Hours '),
            '#placeholder' => t('Employee Type Number Hours to Work'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employeeType->getNumberWorkHours()
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#name' => 'save',
            '#value' => t('Save')
        ];
        
        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        
        $this->employeeType->setName($values['EmployeeTypeName']);
        $this->employeeType->setDescription($values['EmployeeTypeDescription']);
        $this->employeeType->setSalary($values['EmployeeTypeSalary']);
        $this->employeeType->setHourValue($values['EmployeeTypeHourValue']);
        $this->employeeType->setTransportSubsidy($values['EmployeeTypeTramsportationSubsidy']);
        $this->employeeType->setSocialSecurity($values['EmployeeTypeSocialSecuriity']);
        $this->employeeType->setNumberWorkHours($values['EmployeeTypeNumberHours']);
        $valid = $this->employeeType->validate();
        
        /*
         * TODO: Find the way to set message error at the fields tooltip.
         */
        
        if (! empty($valid)) {
            foreach ($valid as $error) {
                $form_state->setErrorByName($error['name'], $error['message']);
            }
        }
        
        return;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        
        if(!$this->employeeType->getId()){
            $isSaved = $this->employeeType->save();
            
            if ($isSaved) {
                drupal_set_message('Employee Type Successful Saved');
            } else {
                drupal_set_message('Employee Type was not Saved','error');
            }
        } else {
            $isUpdated = $this->employeeType->update();
            
            if ($isUpdated) {
                drupal_set_message('Employee Type Successful Saved');
            } else {
                drupal_set_message('Employee Type was not Saved','error');
            }
        }
        
        
        
    }
}


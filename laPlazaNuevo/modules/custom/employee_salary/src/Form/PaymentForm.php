<?php
namespace Drupal\employee_salary\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\employee_salary\Model\Loan;
use Drupal\employee_salary\Model\Payment;

class PaymentForm extends FormBase
{

    /**
     *
     * @var \Drupal\employee_salary\Model\Payment
     */
    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public static function create(ContainerInterface $container)
    {
        return new static($container->get('employee_salary.payment'));
    }

    protected function getEditableConfigNames()
    {
        return [
            'employee_salary.config'
        ];
    }

    public function getFormId()
    {
        return 'payment_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        
        /*
         * TODO: Change the textfield for autocomplete
         */
        
        $form['loan'] = [
            '#type' => 'select',
            '#title' => $this->t('Loan '),
            '#options' => Loan::listLoansDescriptions(),
        ];
        
        $form['amount'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Pay Amount '),
            '#placeholder' => t('Amount To Pay a Loan'),
            '#maxlength' => 255,
            '#size' => 50
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#name' => 'save',
            '#value' => t('Save')
        ];
        
        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        
        $this->payment->setLoan($values['loan']);
        $this->payment->setAmount($values['amount']);
        $this->payment->setType('maked');
        $valid = $this->payment->validate();
        
        /*
         * TODO: Find the way to set message error at the fields tooltip.
         */
        
        if (! empty($valid)) {
            foreach ($valid as $error) {
                $form_state->setErrorByName($error['name'], $error['message']);
            }
        }
        
        return;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $isSaved = $this->payment->save();
        
        if ($isSaved) {
            drupal_set_message('Payment Successful made');
        } else {
            drupal_set_message('Payment was not made','error');
        }
        
    }
    
    
    
}


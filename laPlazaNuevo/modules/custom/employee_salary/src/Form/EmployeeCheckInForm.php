<?php
namespace Drupal\employee_salary\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\employee_salary\Model\Employee;
use Drupal\employee_salary\Model\WorkTime;

class EmployeeCheckInForm extends FormBase
{

    public function getFormId()
    {
        return 'employee_check_in_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['Employee'] = [
            '#type' => 'select',
            '#options' => Employee::listEmployeeNames(),
            '#empty_option' => $this->t('Select Employee'),
            '#required' => TRUE,
            '#cache' => [
                'disabled' => TRUE
            ],
        ];
        
        /*
         * TODO: Make this event ajax
         */
        
        $form['submit'] = [
            '#type' => 'submit',
            '#name' => 'checkin',
            '#value' => t('Check In')
        ];
        
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        
        $workTime = new WorkTime();
        $workTime->setEmployee($values['Employee']);
        $isSaved = $workTime->save();
        
        if ($isSaved) {
            drupal_set_message('Employee Successful Checked In');
        } else {
            drupal_set_message('Employee was not Checked In','error');
        }
        
    }

    
}


<?php
namespace Drupal\employee_salary\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\employee_salary\Util\Festivos;

class ConfigForm extends ConfigFormBase
{

    protected function getEditableConfigNames()
    {
        return [
            'employee_salary.config'
        ];
    }

    public function getFormId()
    {
        return 'config_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('employee_salary.config');
        
        $form['maxHours'] = [
            '#type' => 'details',
            '#title' => $this->t('Max Hours'),
            '#description' => $this->t('Config timelapse of every gap hour'),
            '#open' => FALSE
        ];
        
        $form['maxHours']['maxExitHour'] = [
            '#type' => 'number',
            '#title' => $this->t('Exit Hour'),
            '#min' => 0,
            '#max' => 23,
            '#default_value' => $config->get('maxExitHour'),
            '#required' => TRUE
        ];
        
        $form['gapHours'] = [
            '#type' => 'details',
            '#title' => $this->t('Gap Hours'),
            '#description' => $this->t('Config timelapse of every gap hour'),
            '#open' => FALSE
        ];
        
        $form['gapHours']['gapHoursTabs'] = array(
            '#type' => 'vertical_tabs',
            '#default_tab' => 'edit-publication'
        );
        
        $form['gapHours']['normalHours'] = [
            '#type' => 'details',
            '#title' => $this->t('Normal Hours'),
            '#description' => $this->t('Gap of normal hours'),
            '#open' => TRUE,
            '#group' => 'gapHoursTabs'
        ];
        
        $form['gapHours']['normalHours']['normalHoursStart'] = [
            '#type' => 'number',
            '#title' => $this->t('Start'),
            '#min' => 0,
            '#max' => 23,
            '#default_value' => $config->get('normalHoursStart'),
            '#required' => TRUE
        ];
        
        $form['gapHours']['normalHours']['normalHoursEnd'] = [
            '#type' => 'number',
            '#title' => $this->t('End'),
            '#min' => 0,
            '#max' => 23,
            '#default_value' => $config->get('normalHoursEnd'),
            '#required' => TRUE
        ];
        
        $form['gapHours']['nightlyHours'] = [
            '#type' => 'details',
            '#title' => $this->t('nightly Hours'),
            '#description' => $this->t('Gap of nightly hours'),
            '#open' => TRUE,
            '#group' => 'gapHoursTabs'
        ];
        
        $form['gapHours']['nightlyHours']['nightlyHoursStart'] = [
            '#type' => 'number',
            '#title' => $this->t('Start'),
            '#min' => 0,
            '#max' => 23,
            '#default_value' => $config->get('nightlyHoursStart'),
            '#required' => TRUE
        ];
        
        $form['gapHours']['nightlyHours']['nightlyHoursEnd'] = [
            '#type' => 'number',
            '#title' => $this->t('End'),
            '#min' => 0,
            '#max' => 23,
            '#default_value' => $config->get('nightlyHoursEnd'),
            '#required' => TRUE
        ];
        
        $form['surcharges'] = [
            '#type' => 'details',
            '#title' => $this->t('Surcharges'),
            '#description' => $this->t('Config Surcharges'),
            '#open' => FALSE
        ];
        
        $form['surcharges']['diurnalNormal'] = [
            '#type' => 'number',
            '#title' => $this->t('Diurnal Normal'),
            '#min' => 0,
            '#max' => 23,
            '#step' => 'any',
            '#default_value' => $config->get('diurnalNormal'),
            '#required' => TRUE
        ];
        
        $form['surcharges']['nocturnalNormal'] = [
            '#type' => 'number',
            '#title' => $this->t('Nocturnal Normal'),
            '#min' => 0,
            '#max' => 23,
            '#step' => 'any',
            '#default_value' => $config->get('nocturnalNormal'),
            '#required' => TRUE
        ];
        
        $form['surcharges']['diurnalHoliday'] = [
            '#type' => 'number',
            '#title' => $this->t('Diurnal Holiday'),
            '#min' => 0,
            '#max' => 23,
            '#step' => 'any',
            '#default_value' => $config->get('diurnalHoliday'),
            '#required' => TRUE
        ];
        
        $form['surcharges']['nocturnalHoliday'] = [
            '#type' => 'number',
            '#title' => $this->t('Nocturnal Holiday'),
            '#min' => 0,
            '#max' => 23,
            '#step' => 'any',
            '#default_value' => $config->get('nocturnalHoliday'),
            '#required' => TRUE
        ];
        
        $form['holydays'] = [
            '#type' => 'details',
            '#title' => $this->t('Holydays'),
            '#description' => $this->t('List of Holydays'),
            '#open' => FALSE
        ];
        
        $festivos = new Festivos();
        $festivos->calcularFestivos();
        
        $rows = [];
        
        $holidayNumber = 0;
        foreach ($festivos->festivos as $year => $months) {
            ksort($months);
            foreach ($months as $month => $days) {
                ksort($days);
                foreach ($days as $day => $value) {
                    if ($value) {
                        $date = new \DateTime();
                        $date->setDate($year, $month, $day);
                        
                        $rows[$year . $month . $day] = [
                            'day' => [
                                'data' => $date->format('l')
                            ],
                            'date' => [
                                'data' => $date->format('Y-m-d')
                            ]
                        ];
                    }
                }
            }
        }
        
        $form['holydays']['table'] = [
            '#type' => 'table',
            '#header' => [
                'day' => $this->t('Día'),
                'date' => $this->t('Fecha')
            ],
            '#rows' => $rows,
            '#empty' => t('No users found')
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#name' => 'save',
            '#value' => t('Save')
        ];
        
        /*
         * TODO: Set the holydays list in this form, with pagger if is needed
         */
        
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        
        \Drupal::configFactory()->getEditable('employee_salary.config')
        ->set('maxExitHour',$values['maxExitHour'])
            ->set('normalHoursStart', $values['normalHoursStart'])
            ->set('normalHoursEnd', $values['normalHoursEnd'])
            ->set('nightlyHoursStart', $values['nightlyHoursStart'])
            ->set('nightlyHoursEnd', $values['nightlyHoursEnd'])
            ->set('diurnalNormal', $values['diurnalNormal'])
            ->set('nocturnalNormal', $values['nocturnalNormal'])
            ->set('diurnalHoliday', $values['diurnalHoliday'])
            ->set('nocturnalHoliday', $values['nocturnalHoliday'])
            ->save();
        
        drupal_set_message('Configs are successful saveded');
    }
}
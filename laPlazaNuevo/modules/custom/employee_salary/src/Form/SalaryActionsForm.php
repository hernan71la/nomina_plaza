<?php
namespace Drupal\employee_salary\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\employee_salary\Util\PrintUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SalaryActionsForm extends FormBase
{

    protected $printUtil;

    private $employeeDetails;

    private $employeeType;

    private $salary;

    private $extraHoursDetails;

    private $totalDiscount;

    private $discounts;

    public function __construct(PrintUtil $printUtil)
    {
        $this->printUtil = $printUtil;
    }

    public static function create(ContainerInterface $container)
    {
        return new static($container->get('employee_salary.printer'));
    }

    protected function getEditableConfigNames()
    {
        return [
            'employee_salary.config'
        ];
    }

    public function getFormId()
    {
        return 'salary_actions_form';
    }

    public function setSalaryInfo($employeeDetails, $employeeType, $salary, $extraHoursDetails, $totalDiscount, $discounts)
    {
        $this->employeeDetails = $employeeDetails;
        $this->employeeType = $employeeType;
        $this->salary = $salary;
        $this->extraHoursDetails = $extraHoursDetails;
        $this->totalDiscount = $totalDiscount;
        $this->discounts = $discounts;
    }

    public function buildForm(array $form, FormStateInterface $form_state, $id = null)
    {
        $form['generatePdf'] = [
            '#type' => 'button',
            '#name' => 'save',
            '#value' => t('PDF'),
            '#ajax' => array(
                'callback' => array(
                    $this,
                    'saveCallback'
                )
            )
        ];
        
        $form['print'] = [
            '#type' => 'button',
            '#name' => 'print',
            '#value' => t('Imprimir'),
            '#ajax' => array(
                'callback' => array(
                    $this,
                    'printCallback'
                )
            )
        ];
        
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {}

    public function saveCallback()
    {}

    public function printCallback()
    {
        $this->printUtil->print($this->employeeDetails, $this->employeeType, $this->salary,
            $this->extraHoursDetails,
            $this->totalDiscount,
            $this->discounts
            );
        
    }
    

}
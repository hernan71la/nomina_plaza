<?php
namespace Drupal\employee_salary\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\employee_salary\Model\Employee;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\employee_salary\Model\WorkTime;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\employee_salary\Model\Salary;

class SalaryForm extends FormBase
{

    /**
     *
     * @var \Drupal\employee_salary\Model\WorkTime
     */
    protected $workTime;

    /**
     *
     * @var \Drupal\employee_salary\Model\Employee
     */
    protected $employee;

    /**
     * Drupal\user\PrivateTempStoredefinition.
     *
     * @var Drupal\user\PrivateTempStore
     */
    protected $tempStore;

    public function __construct(WorkTime $workTime, Employee $employee, PrivateTempStoreFactory $tempStore)
    {
        $this->workTime = $workTime;
        $this->employee = $employee;
        $this->tempStore = $tempStore->get('employee_salary_session');
    }

    public static function create(ContainerInterface $container)
    {
        return new static($container->get('employee_salary.work_time'), $container->get('employee_salary.employee'), $container->get('user.private_tempstore'));
    }

    public function getFormId()
    {
        return 'salary_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['Employee'] = [
            '#type' => 'select',
            '#title' => 'Employee',
            '#options' => $this->employee->listAllEmployeeNames(),
            '#empty_option' => $this->t('Select Employee'),
            '#required' => TRUE
        ];
        
        $form['startDate'] = [
            '#type' => 'date',
            '#title' => $this->t('Desde'),
            '#placeholder' => t('Desde'),
            '#date_format' => 'Y-m-d',
            '#required' => TRUE
        ];
        
        $form['endDate'] = [
            '#type' => 'date',
            '#title' => $this->t('Hasta'),
            '#placeholder' => t('Hasta'),
            '#date_format' => 'Y-m-d',
            '#required' => TRUE
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#name' => 'save',
            '#value' => t('Save')
        ];
        
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
       
        
        $salary = new Salary($values['Employee'], $values['startDate'], $values['endDate']);
        $result = $salary->validateSalaryGeneration();
        
        if(empty($result)){
            $form_state->setRedirect('employee_salary.view_salary', [
                'salaryEmployee' => $values['Employee'],
                'salaryStartDate' => $values['startDate'],
                'salaryEndDate' => $values['endDate']
            ]);
        } else {
            kint($result);
            
            die;
        }
        
        
        
        return;
    }
}


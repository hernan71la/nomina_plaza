<?php
namespace Drupal\employee_salary\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\employee_salary\Model\Employee;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\employee_salary\Model\EmployeeType;

class EmployeeForm extends FormBase
{

    /**
     *
     * @var \Drupal\employee_salary\Model\Employee
     */
    protected $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public static function create(ContainerInterface $container)
    {
        return new static($container->get('employee_salary.employee'));
    }

    protected function getEditableConfigNames()
    {
        return [
            'employee_salary.config'
        ];
    }

    public function getFormId()
    {
        return 'employee_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $id = null)
    {
        
        $this->employee->setId($id);
        $this->employee->query();
        
        /*
         * TODO: Add #required at fields need it
         * TODO: List the employee type in the database
         */
        
        $form['EmployeeType'] = [
            '#type' => 'select',
            '#title' => 'Employee Type',
            '#options' => EmployeeType::listEmployeeTypeName(),
            '#empty_option' => $this->t('Select Employee Type'),
            '#default_value' => $this->employee->getType()
        ];
        
        $form['EmployeeName'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Name '),
            '#placeholder' => t('Employee Name'),
            '#maxlength' => 255,
            '#size' => 50, 
            '#default_value' => $this->employee->getName()
        ];
        
        $form['EmployeeLastName'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Lastname '),
            '#placeholder' => t('Employee Lastname'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employee->getLastName()
        ];
        
        $form['EmployeeDocument'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Document '),
            '#placeholder' => t('Employee Document'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employee->getDocument()
        ];
        
        $form['EmployeePhone'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Phone '),
            '#placeholder' => t('Employee Phone number'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employee->getPhone()
        ];
        
        
        /*
         * TODO: add email validation
         */
        
        $form['EmployeeEmail'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Hour Email '),
            '#placeholder' => t('Employee Email Address'),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employee->getEmail()
        ];
        
        $form['EmployeeAddress'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Address '),
            '#placeholder' => t('Employee Address '),
            '#maxlength' => 255,
            '#size' => 50,
            '#default_value' => $this->employee->getAddress()
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#name' => 'save',
            '#value' => t('Save')
        ];
        
        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        
        /*
         * TODO: Study the way to had a select with a object.
         */
        $values = $form_state->getValues();
        
        $this->employee->setType($values['EmployeeType']);
        $this->employee->setName($values['EmployeeName']);
        $this->employee->setLastName($values['EmployeeLastName']);
        $this->employee->setDocument($values['EmployeeDocument']);
        $this->employee->setPhone($values['EmployeePhone']);
        $this->employee->setEmail($values['EmployeeEmail']);
        $this->employee->setAddress($values['EmployeeAddress']);
        $valid = $this->employee->validate();
        
        /*
         * TODO: Find the way to set message error at the fields tooltip.
         */
        
        if (! empty($valid)) {
            foreach ($valid as $error) {
                $form_state->setErrorByName($error['name'], $error['message']);
            }
        }
        
        return;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        
        
        if(!$this->employee->getId()){
            $isSaved = $this->employee->save();
            
            if ($isSaved) {
                drupal_set_message('Employee Successful Saved');
            } else {
                drupal_set_message('Employee was not Saved','error');
            }
        }else {
            $isUpdated = $this->employee->update();
            
            if ($isUpdated) {
                drupal_set_message('Employee Successful Update');
            } else {
                drupal_set_message('Employee was not Saved','error');
            }
        }
        
        
    }
    
    
    
}


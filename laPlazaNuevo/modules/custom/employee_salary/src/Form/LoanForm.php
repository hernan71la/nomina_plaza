<?php
namespace Drupal\employee_salary\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\employee_salary\Model\Loan;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\employee_salary\Model\Employee;

class LoanForm extends FormBase
{

    /**
     *
     * @var \Drupal\employee_salary\Model\Loan
     */
    protected $loan;

    public function __construct(Loan $loan)
    {
        $this->loan = $loan;
    }

    public static function create(ContainerInterface $container)
    {
        return new static($container->get('employee_salary.loan'));
    }

    protected function getEditableConfigNames()
    {
        return [
            'employee_salary.config'
        ];
    }

    public function getFormId()
    {
        return 'loan_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        
        /*
         * TODO: List employees from the database
         */
        $form['Employee'] = [
            '#type' => 'select',
            '#title' => 'Employee',
            '#options' => Employee::listEmployeeNames(),
            '#empty_option' => $this->t('Select Employee')
        ];
        
        $form['loanDescription'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Description '),
            '#placeholder' => t('Loan Description'),
            '#maxlength' => 255,
            '#size' => 50
        ];
        
        $form['loanPayForm'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Pay Form '),
            '#placeholder' => t('Loan Pay Form'),
            '#maxlength' => 255,
            '#size' => 50
        ];
        
        $form['loanNumberOfPays'] = [
            '#type' => 'number',
            '#title' => $this->t('Number Of Pays '),
            '#placeholder' => t('Loan Number of Pays'),
            '#min' => 1,
            '#max' => 20
        ];
        
        $form['loanAmount'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Amount'),
            '#placeholder' => t('Loan Pay Form'),
            '#maxlength' => 255,
            '#size' => 50
        ];
        
        $form['loanDateOfFirstPay'] = [
            '#type' => 'date',
            '#title' => $this->t('Date Of First Pay'),
            '#placeholder' => t('Loan Date Of First Pay'),
        ];
        
        $form['submit'] = [
            '#type' => 'submit',
            '#name' => 'save',
            '#value' => t('Save')
        ];
        
        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        
        $this->loan->setEmployee($values['Employee']);
        $this->loan->setDescription($values['loanDescription']);
        $this->loan->setPayForm($values['loanPayForm']);
        $this->loan->setNumberOfPays($values['loanNumberOfPays']);
        $this->loan->setTotalAmount($values['loanAmount']);
        $this->loan->setFistPayDate($values['loanDateOfFirstPay']);
        $valid = $this->loan->validate();
        
        /*
         * TODO: Find the way to set message error at the fields tooltip.
         */
        
        if (! empty($valid)) {
            foreach ($valid as $error) {
                $form_state->setErrorByName($error['name'], $error['message']);
            }
        }
        
        return;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $isSaved = $this->loan->save();
        
        if ($isSaved) {
            drupal_set_message('Loan Successful Created');
        } else {
            drupal_set_message('Loan was not Created','error');
        }
        
    }
    
    
    
}


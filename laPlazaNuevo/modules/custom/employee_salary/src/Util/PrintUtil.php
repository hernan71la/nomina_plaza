<?php
namespace Drupal\employee_salary\Util;

use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;

class PrintUtil
{

    public function print($employeeDetails, $employeeType, $salary, $extraHoursDetails, $totalDiscount, $discounts)
    {
        try {
            // Enter connector and capability profile (to match your printer)
            $connector = new FilePrintConnector("LPT3");
            
            /* Print a series of receipts containing i18n example strings */
            $printer = new Printer($connector);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_WIDTH);
            $printer->text("   LIQUIDACIÓN\n");
            $printer->selectPrintMode();
            
            $printer->feed();
            
            $printer->setEmphasis(true);
            $printer->text("Tipo Liqudación: ");
            $printer->setEmphasis(false);
            $printer->text($employeeType->getName() . "\n");
            
            $printer->setEmphasis(true);
            $printer->text("Nombre:\n");
            $printer->setEmphasis(false);
            $printer->text($employeeDetails->getName() . ' ' . $employeeDetails->getLastName() . "\n");
            
            $printer->setEmphasis(true);
            $printer->text("Periodo Liquidado:\n");
            $printer->setEmphasis(false);
            $printer->text($salary->getInitialDate() . " - " . $salary->getFinalDate() . "\n");
            
            $printer->feed();
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $printer->text("Ingresos\n");
            $printer->selectPrintMode();
            
            $printer->setEmphasis(true);
            $printer->text("Salario:  ");
            $printer->setEmphasis(false);
            $printer->text("$ " . $salary->getSalaryAmount() . "\n");
            
            $printer->setEmphasis(true);
            $printer->text("Horas Extras:  ");
            $printer->setEmphasis(false);
            $printer->text("$ " . $salary->getTotalWorkedAmount() . "\n");
            
            $printer->setEmphasis(true);
            $printer->text("Sub. Transporte:  ");
            $printer->setEmphasis(false);
            $printer->text("$ " . $salary->getTransportationValue() . "\n");
            
            $printer->feed();
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $printer->text("Deducciones\n");
            $printer->selectPrintMode();
            
            $printer->setEmphasis(true);
            $printer->text("Seguridad Social:  ");
            $printer->setEmphasis(false);
            $printer->text("$ " . $salary->getSecurityValue() . "\n");
            
            $printer->setEmphasis(true);
            $printer->text("Prestamos:  ");
            $printer->setEmphasis(false);
            $printer->text("$ " . $totalDiscount . "\n");
            
            $printer->feed();
            
			$total = $salary->getTotalAmount() - $totalDiscount;
			
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_EMPHASIZED);
            $printer->text("Total:         ");
            $printer->text("$ " . $total . "\n");
            $printer->selectPrintMode();
            
            $printer->feed(2);
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $printer->text("Detalle Horas\n");
            $printer->selectPrintMode();
            
            $printer->setEmphasis(true);
            $printer->text("Diurnas:  ");
            $printer->setEmphasis(false);
            $printer->text($extraHoursDetails['diurnalTime'] . "\n");
            
            $printer->setEmphasis(true);
            $printer->text("Nocturnas:  ");
            $printer->setEmphasis(false);
            $printer->text($extraHoursDetails['nocturnalTime'] . "\n");
            
            $printer->setEmphasis(true);
            $printer->text("Diurnas Festivas:  ");
            $printer->setEmphasis(false);
            $printer->text($extraHoursDetails['holyDiurnalTime'] . "\n");
            
            $printer->setEmphasis(true);
            $printer->text("Nocturnas Festivas:  ");
            $printer->setEmphasis(false);
            $printer->text($extraHoursDetails['holyNocturnalTime'] . "\n");
            
            $printer->feed();
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $printer->text("Detalle Prestamos\n");
            $printer->selectPrintMode();
            
            foreach ($discounts as $discount => $value) {
                $printer->setEmphasis(true);
                $printer->text($discount . ":  ");
                $printer->setEmphasis(false);
                $printer->text("$ " . $value . "\n");
            }
            
            $printer->cut();
            /* Close printer */
            $printer->close();
        } catch (\Exception $e) {
            echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
        }
    }
    
}


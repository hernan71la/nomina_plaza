<?php
namespace Drupal\employee_salary\Util;

class Validator
{

    const DOCUMENT_PATTERN = '/^(([1-9][0-9]{0,7}|(\A[1][0-9]{9})))$/';

    /*
     * TODO: Set the Phone pattern rergular expression
     * TODO: Set the Email pattern rergular expression
     */
    
    const PHONE_PATTERN = '';

    const EMAIL_PATTERN = '';

    public static function validatePattern($value, $pattern)
    {
        $isValid = preg_match($pattern, $value);
        return $isValid;
    }
}


<?php
namespace Drupal\employee_salary\Util;

class Date
{
    
    public static function calculateTimes()
    {
        $startTime = new \DateTime();
        $startTime->setTimestamp(1515009882);
        
        $endTime = new \DateTime();
        $endTime->setTimestamp(1515036617);
        
        kint($startTime->format('Y-m-d H:i:s'), $endTime->format('Y-m-d H:i:s'));
        
        if ($startTime < $endTime) {
            $diff = $startTime->diff($endTime, false);
            
            for ($i = 1; $i <= $diff->d; $i ++) {
                $start = clone $startTime;
                $startTime->add(new \DateInterval('P1D'));
                $end = clone $startTime;
                
                $times = Date::getHours($start, $end);
                
                kint($times);
            }
            
            $times = Date::getHours($startTime, $endTime);
            
            kint($times);
        }
    }

    public static function getHours($startTime, $endTime)
    {
        $config = \Drupal::config('employee_salary.config');
        
        $diurnalHours = 0;
        $diurnalMinutes = 0;
        $nocturnalHours = 0;
        $nocturnalMinutes = 0;
        $holydayDiurnalHours = 0;
        $holydayDiurnalMinutes = 0;
        $holydayNocturnalHours = 0;
        $holydayNocturnalMinutes = 0;
        
        $festivo = new Festivos();
        
        $diurnalStart = new \DateTime($startTime->format('Y-m-d'));
        $diurnalStart->setTime($config->get('normalHoursStart'), 0);
        
        $diurnalEnd = new \DateTime($startTime->format('Y-m-d'));
        $diurnalEnd->setTime($config->get('normalHoursEnd'), 0);
        
        $nocturnalStart = new \DateTime($startTime->format('Y-m-d'));
        $nocturnalStart->setTime($config->get('nightlyHoursStart'), 0);
        
        if ($endTime->format('Y-m-d') == $startTime->format('Y-m-d')) {
            $nocturnalEnd = new \DateTime($endTime->format('Y-m-d'));
            $nocturnalEnd->setTime($config->get('nightlyHoursEnd'), 0);
            $nocturnalEnd->add(new \DateInterval('P1D'));
        } else {
            $nocturnalEnd = new \DateTime($endTime->format('Y-m-d'));
            $nocturnalEnd->setTime($config->get('nightlyHoursEnd'), 0);
        }
        
        if ($startTime >= $diurnalStart && $startTime <= $diurnalEnd) {
            if ($endTime <= $diurnalEnd) {
                $diurnal = $startTime->diff($endTime);
                if (! $festivo->esFestivo($startTime->format('Y-m-d')) && ! $festivo->esDomingo($startTime)) {
                    $diurnalHours = $diurnal->h;
                    $diurnalMinutes = $diurnal->i;
                } else {
                    $holydayDiurnalHours = $diurnal->h;
                    $holydayDiurnalMinutes = $diurnal->i;
                }
            } else if ($endTime >= $nocturnalStart) {
                if ($endTime <= $nocturnalEnd) {
                    kint('et < ne');
                    $diurnal = $startTime->diff($diurnalEnd);
                    if (! $festivo->esFestivo($startTime->format('Y-m-d')) && ! $festivo->esDomingo($startTime)) {
                        $diurnalHours = $diurnal->h;
                        $diurnalMinutes = $diurnal->i;
                    } else {
                        $holydayDiurnalHours = $diurnal->h;
                        $holydayDiurnalMinutes = $diurnal->i;
                    }
                    
                    if (! $festivo->esFestivo($nocturnalStart->format('Y-m-d')) && ! $festivo->esDomingo($nocturnalStart)) {
                        if (! $festivo->esFestivo($endTime->format('Y-m-d')) && ! $festivo->esDomingo($endTime)) {
                            
                            $nocturnal = $nocturnalStart->diff($endTime);
                            $nocturnalHours = $nocturnal->h;
                            $nocturnalMinutes = $nocturnal->i;
                        } else {
                            $dayStart = clone $startTime;
                            $dayStart->add(new \DateInterval('P1D'));
                            $dayStart->setTime(0, 0);
                            
                            if ($endTime >= $dayStart) {
                                $nocturnal = $nocturnalStart->diff($dayStart);
                                $nocturnalHours = $nocturnal->h;
                                $nocturnalMinutes = $nocturnal->i;
                                
                                $holyNocturnal = $dayStart->diff($endTime, false);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                            } else {
                                $holyNocturnal = $nocturnalStart->diff($endTime, false);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                            }
                        }
                    } else {
                        $dayStart = clone $startTime;
                        $dayStart->add(new \DateInterval('P1D'));
                        $dayStart->setTime(0, 0);
                        
                        if (! $festivo->esFestivo($endTime->format('Y-m-d')) && ! $festivo->esDomingo($endTime)) {
                            $holyNocturnal = $nocturnalStart->diff($dayStart, false);
                            $holydayNocturnalHours = $holyNocturnal->h;
                            $holydayNocturnalMinutes = $holyNocturnal->i;
                            
                            $nocturnal = $dayStart->diff($endTime);
                            $nocturnalHours = $nocturnal->h;
                            $nocturnalMinutes = $nocturnal->i;
                        } else {
                            if ($endTime >= $dayStart) {
                                $holyNocturnal = $nocturnalStart->diff($dayStart);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                                
                                $nocturnal = $dayStart->diff($endTime, false);
                                $nocturnalHours = $nocturnal->h;
                                $nocturnalMinutes = $nocturnal->i;
                            } else {
                                $holyNocturnal = $nocturnalStart->diff($endTime, false);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                            }
                        }
                    }
                } else {
                    $diurnal = $startTime->diff($diurnalEnd);
                    if (! $festivo->esFestivo($startTime->format('Y-m-d')) && ! $festivo->esDomingo($startTime)) {
                        $diurnalHours = $diurnal->h;
                        $diurnalMinutes = $diurnal->i;
                    } else {
                        $holydayDiurnalHours = $diurnal->h;
                        $holydayDiurnalMinutes = $diurnal->i;
                    }
                    
                    $dayStart = clone $startTime;
                    $dayStart->add(new \DateInterval('P1D'));
                    $dayStart->setTime(0, 0);
                    
                    if (! $festivo->esFestivo($nocturnalStart->format('Y-m-d')) && ! $festivo->esDomingo($nocturnalStart)) {
                        if (! $festivo->esFestivo($nocturnalEnd->format('Y-m-d')) && ! $festivo->esDomingo($nocturnalEnd)) {
                            $nocturnal = $nocturnalStart->diff($nocturnalEnd);
                            $nocturnalHours = $nocturnal->h;
                            $nocturnalMinutes = $nocturnal->i;
                        } else {
                            $nocturnal = $nocturnalStart->diff($dayStart);
                            $nocturnalHours = $nocturnal->h;
                            $nocturnalMinutes = $nocturnal->i;
                            
                            $holyNocturnal = $dayStart->diff($nocturnalEnd, false);
                            $holydayNocturnalHours = $holyNocturnal->h;
                            $holydayNocturnalMinutes = $holyNocturnal->i;
                        }
                    } else {
                        $holyNocturnal = $nocturnalStart->diff($dayStart, false);
                        $holydayNocturnalHours = $holyNocturnal->h;
                        $holydayNocturnalMinutes = $holyNocturnal->i;
                        
                        $nocturnal = $dayStart->diff($nocturnalEnd);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                    }
                    
                    if (! $festivo->esFestivo($nocturnalEnd->format('Y-m-d')) && ! $festivo->esDomingo($nocturnalEnd)) {
                        $diurnalNext = $nocturnalEnd->diff($endTime);
                        $diurnalHours += $diurnalNext->h;
                        $diurnalMinutes += $diurnalNext->i;
                    } else {
                        $holyDiurnalNext = $nocturnalEnd->diff($endTime);
                        $holydayDiurnalHours += $holyDiurnalNext->h;
                        $holydayDiurnalMinutes += $holyDiurnalNext->i;
                    }
                }
            }
        } else if ($startTime >= $nocturnalStart && $startTime <= $nocturnalEnd) {
            
            $diurnalStart->add(new \DateInterval('P1D'));
            $diurnalEnd->add(new \DateInterval('P1D'));
            
            $dayStart = clone $nocturnalEnd;
            $dayStart->setTime(0, 0);
            
            if ($endTime < $nocturnalEnd) {
                
                if (! $festivo->esFestivo($nocturnalStart->format('Y-m-d')) && ! $festivo->esDomingo($nocturnalStart)) {
                    if (! $festivo->esFestivo($nocturnalEnd->format('Y-m-d')) && ! $festivo->esDomingo($nocturnalEnd)) {
                        $nocturnal = $startTime->diff($endTime);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                    } else {
                        $nocturnal = $startTime->diff($dayStart);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                        
                        $holyNocturnal = $dayStart->diff($endTime);
                        $holydayNocturnalHours += $holyNocturnal->h;
                        $holydayNocturnalMinutes += $holyNocturnal->i;
                    }
                } else {
                    if (! $festivo->esFestivo($nocturnalEnd->format('Y-m-d')) && ! $festivo->esDomingo($nocturnalEnd)) {
                        $holyNocturnal = $startTime->diff($dayStart);
                        $holydayNocturnalHours += $holyNocturnal->h;
                        $holydayNocturnalMinutes += $holyNocturnal->i;
                        
                        $nocturnal = $dayStart->diff($endTime);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                    } else {
                        $holyNocturnal = $startTime->diff($endTime);
                        $holydayNocturnalHours += $holyNocturnal->h;
                        $holydayNocturnalMinutes += $holyNocturnal->i;
                    }
                }
            } else if ($endTime >= $diurnalStart) {
                $dayStart = clone $nocturnalEnd;
                $dayStart->setTime(0, 0);
                if ($endTime <= $diurnalEnd) {
                    if ($startTime < $dayStart) {
                        if (! $festivo->esFestivo($startTime->format('Y-m-d')) && ! $festivo->esDomingo($startTime)) {
                            if (! $festivo->esFestivo($diurnalStart->format('Y-m-d')) && ! $festivo->esDomingo($diurnalStart)) {
                                $nocturnal = $startTime->diff($nocturnalEnd);
                                $nocturnalHours = $nocturnal->h;
                                $nocturnalMinutes = $nocturnal->i;
                                
                                $diurnal = $diurnalStart->diff($endTime);
                                $diurnalHours = $diurnal->h;
                                $diurnalMinutes = $diurnal->i;
                            } else {
                                $nocturnal = $startTime->diff($dayStart);
                                $nocturnalHours = $nocturnal->h;
                                $nocturnalMinutes = $nocturnal->i;
                                
                                $holyNocturnal = $dayStart->diff($nocturnalEnd);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                                
                                $holyDiurnal = $diurnalStart->diff($endTime);
                                $holydayDiurnalHours += $holyDiurnal->h;
                                $holydayDiurnalMinutes += $holyDiurnal->i;
                            }
                        } else {
                            if (! $festivo->esFestivo($diurnalStart->format('Y-m-d')) && ! $festivo->esDomingo($diurnalStart)) {
                                $holyNocturnal = $startTime->diff($dayStart);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                                
                                $nocturnal = $dayStart->diff($nocturnalEnd);
                                $nocturnalHours = $nocturnal->h;
                                $nocturnalMinutes = $nocturnal->i;
                                
                                $diurnal = $diurnalStart->diff($endTime);
                                $diurnalHours = $diurnal->h;
                                $diurnalMinutes = $diurnal->i;
                            } else {
                                $holyNocturnal = $startTime->diff($nocturnalEnd);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                                
                                $holyDiurnal = $diurnalStart->diff($endTime);
                                $holydayDiurnalHours += $holyDiurnal->h;
                                $holydayDiurnalMinutes += $holyDiurnal->i;
                            }
                        }
                    }
                } else {
                    
                    if ($startTime < $dayStart) {
                        if (! $festivo->esFestivo($startTime->format('Y-m-d')) && ! $festivo->esDomingo($startTime)) {
                            if (! $festivo->esFestivo($diurnalStart->format('Y-m-d')) && ! $festivo->esDomingo($diurnalStart)) {
                                $nocturnal = $startTime->diff($nocturnalEnd);
                                $nocturnalHours = $nocturnal->h;
                                $nocturnalMinutes = $nocturnal->i;
                                
                                $diurnal = $diurnalStart->diff($diurnalEnd);
                                $diurnalHours = $diurnal->h;
                                $diurnalMinutes = $diurnal->i;
                                
                                $nocturnalNext = $nocturnalStart->diff($endTime);
                                $nocturnalHours += $nocturnalNext->h;
                                $nocturnalMinutes += $nocturnalNext->i;
                            } else {
                                $nocturnal = $startTime->diff($dayStart);
                                $nocturnalHours = $nocturnal->h;
                                $nocturnalMinutes = $nocturnal->i;
                                
                                $holyNocturnal = $dayStart->diff($nocturnalEnd);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                                
                                $holyDiurnal = $diurnalStart->diff($endTime);
                                $holydayDiurnalHours += $holyDiurnal->h;
                                $holydayDiurnalMinutes += $holyDiurnal->i;
                            }
                        } else {
                            if (! $festivo->esFestivo($diurnalStart->format('Y-m-d')) && ! $festivo->esDomingo($diurnalStart)) {
                                $holyNocturnal = $startTime->diff($dayStart);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                                
                                $nocturnal = $dayStart->diff($nocturnalEnd);
                                $nocturnalHours = $nocturnal->h;
                                $nocturnalMinutes = $nocturnal->i;
                                
                                $diurnal = $diurnalStart->diff($endTime);
                                $diurnalHours = $diurnal->h;
                                $diurnalMinutes = $diurnal->i;
                            } else {
                                $holyNocturnal = $startTime->diff($nocturnalEnd);
                                $holydayNocturnalHours = $holyNocturnal->h;
                                $holydayNocturnalMinutes = $holyNocturnal->i;
                                
                                $holyDiurnal = $diurnalStart->diff($endTime);
                                $holydayDiurnalHours += $holyDiurnal->h;
                                $holydayDiurnalMinutes += $holyDiurnal->i;
                            }
                        }
                    }
                }
            }
        }
        
        return [
            'diurnalHours' => $diurnalHours,
            'diurnalMinutes' => $diurnalMinutes,
            'nocturnalHours' => $nocturnalHours,
            'nocturnalMinutes' => $nocturnalMinutes,
            'holydayDiurnalHours' => $holydayDiurnalHours,
            'holydayDiurnalMinutes' => $holydayDiurnalMinutes,
            'holydayNocturnalHours' => $holydayNocturnalHours,
            'holydayNocturnalMinutes' => $holydayNocturnalMinutes
        ];
    }
    
    public static function calculateDiurnalHours($startTime, $endTime){
        
        $config = \Drupal::config('employee_salary.config');
        
        $festivo = new Festivos();
        
        $diurnalHours = 0;
        $diurnalMinutes = 0;
        $holydayDiurnalHours = 0;
        $holydayDiurnalMinutes = 0;
        
        $diurnalStart = new \DateTime($startTime->format('Y-m-d'));
        $diurnalStart->setTime($config->get('normalHoursStart'), 0);
        
        $diurnalEnd = new \DateTime($startTime->format('Y-m-d'));
        $diurnalEnd->setTime($config->get('normalHoursEnd'), 0);
        
        if($startTime >= $diurnalStart && $startTime <= $diurnalEnd){
            if($endTime < $diurnalEnd){
                $dateDiff = $startTime->diff($endTime);
                if(!$festivo->esDomingo($startTime) && !$festivo->esFestivo($startTime->format('Y-m-d'))){                    
                    $diurnalHours = $dateDiff->h;
                    $diurnalMinutes = $dateDiff->i;
                }else {
                    $holydayDiurnalHours = $dateDiff->h;
                    $holydayDiurnalMinutes = $dateDiff->i;
                }
            }else {
                $dateDiff = $startTime->diff($diurnalEnd);
                if(!$festivo->esDomingo($startTime) && !$festivo->esFestivo($startTime->format('Y-m-d'))){                     
                    $diurnalHours = $dateDiff->h;
                    $diurnalMinutes = $dateDiff->i;
                } else {
                    $holydayDiurnalHours = $dateDiff->h;
                    $holydayDiurnalMinutes = $dateDiff->i;
                }
            }
        }
        
        return [
            'diurnalHours' => $diurnalHours,
            'diurnalMinutes' => $diurnalMinutes,
            'holydayDiurnalHours' => $holydayDiurnalHours,
            'holydayDiurnalMinutes' => $holydayDiurnalMinutes
        ];
        
    }
    
    public static function calculateNocturnalHours($startTime, $endTime){
        
        $config = \Drupal::config('employee_salary.config');
        
        $festivo = new Festivos();
        
        $nocturnalHours = 0;
        $nocturnalMinutes = 0;
        $holydayNocturnalHours = 0;
        $holydayNocturnalMinutes = 0;
        
        $diurnalStart = new \DateTime($startTime->format('Y-m-d'));
        $diurnalStart->setTime($config->get('normalHoursStart'), 0);
        
        $nocturnalStart = new \DateTime($startTime->format('Y-m-d'));
        $nocturnalStart->setTime($config->get('nightlyHoursStart'), 0);
        
        $dayStart = clone $startTime;
        $dayStart->add(new \DateInterval('P1D'));
        $dayStart->setTime(0, 0);
        
        $nocturnalEnd = new \DateTime($endTime->format('Y-m-d'));
        $nocturnalEnd->setTime($config->get('nightlyHoursEnd'), 0);
        $nocturnalEnd->add(new \DateInterval('P1D'));
        
        if($startTime < $diurnalStart){
            $nocturnalStart->sub(new \DateInterval('P1D'));
            $dayStart->sub(new \DateInterval('P1D'));
            $nocturnalEnd->sub(new \DateInterval('P1D'));
        }
        if($endTime > $nocturnalStart){
            if($startTime >= $nocturnalStart){
                if(!$festivo->esDomingo($startTime) && !$festivo->esFestivo($startTime->format('Y-m-d'))){
                    if(!$festivo->esDomingo($nocturnalEnd) && !$festivo->esFestivo($nocturnalEnd->format('Y-m-d'))){
                        $nocturnal = $startTime->diff($endTime);
                        
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                    } else {
                        $nocturnal = $startTime->diff($dayStart);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                        
                        $holyNocturnal = $dayStart->diff($endTime);
                        $holydayNocturnalHours = $holyNocturnal->h;
                        $holydayNocturnalMinutes = $holyNocturnal->i;
                        
                    }
                } else {
                    if(!$festivo->esDomingo($nocturnalEnd) && !$festivo->esFestivo($nocturnalEnd->format('Y-m-d'))){
                        $holyNocturnal = $startTime->diff($dayStart);
                        $holydayNocturnalHours = $holyNocturnal->h;
                        $holydayNocturnalMinutes = $holyNocturnal->i;
                        
                        $nocturnal = $dayStart->diff($endTime);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                    } else {
                        $holyNocturnal = $startTime->diff($endTime);
                        $holydayNocturnalHours = $holyNocturnal->h;
                        $holydayNocturnalMinutes = $holyNocturnal->i;
                    }
                }
            } else {
                if(!$festivo->esDomingo($nocturnalStart) && !$festivo->esFestivo($nocturnalStart->format('Y-m-d'))){
                    if(!$festivo->esDomingo($nocturnalEnd) && !$festivo->esFestivo($nocturnalEnd->format('Y-m-d'))){
                        $nocturnal = $nocturnalStart->diff($endTime);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                    } else {
                        $nocturnal = $nocturnalStart->diff($dayStart);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                        
                        $holyNocturnal = $dayStart->diff($endTime);
                        $holydayNocturnalHours = $holyNocturnal->h;
                        $holydayNocturnalMinutes = $holyNocturnal->i;
                        
                    }
                } else {
                    if(!$festivo->esDomingo($nocturnalEnd) && !$festivo->esFestivo($nocturnalEnd->format('Y-m-d'))){
                        $holyNocturnal = $nocturnalStart->diff($dayStart);
                        $holydayNocturnalHours = $holyNocturnal->h;
                        $holydayNocturnalMinutes = $holyNocturnal->i;
                        
                        $nocturnal = $dayStart->diff($endTime);
                        $nocturnalHours = $nocturnal->h;
                        $nocturnalMinutes = $nocturnal->i;
                    } else {
                        $holyNocturnal = $nocturnalStart->diff($endTime);
                        $holydayNocturnalHours = $holyNocturnal->h;
                        $holydayNocturnalMinutes = $holyNocturnal->i;
                    }
                }
            }
        }
        
        
        return [
            'nocturnalHours' => $nocturnalHours,
            'nocturnalMinutes' => $nocturnalMinutes,
            'holydayNocturnalHours' => $holydayNocturnalHours,
            'holydayNocturnalMinutes' => $holydayNocturnalMinutes
        ];
    }

}


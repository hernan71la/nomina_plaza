<?php
namespace Drupal\employee_salary\Controller;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\employee_salary\Model\WorkTime;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use DateTime;
use Drupal\employee_salary\Model\Employee;
use Drupal\employee_salary\Model\EmployeeType;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\employee_salary\Util\Date;

class WorkTimeController extends ControllerBase
{

    /**
     *
     * @var \Drupal\employee_salary\Model\WorkTime
     */
    protected $workTime;

    /**
     *
     * @var \Drupal\employee_salary\Model\Employee
     */
    protected $employee;

    /**
     *
     * @var \Drupal\employee_salary\Model\EmployeeType
     */
    protected $employeeType;

    /**
     * Symfony\Component\HttpFoundation\RequestStack definition.
     *
     * @var object
     */
    protected $requestStack;

    /**
     * Drupal\user\PrivateTempStoredefinition.
     *
     * @var Drupal\user\PrivateTempStore
     */
    protected $tempStore;

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public function __construct(RequestStack $requestStack, WorkTime $workTime, Employee $employee, EmployeeType $employeeType, PrivateTempStoreFactory $tempStore)
    {
        $this->requestStack = $requestStack;
        $this->workTime = $workTime;
        $this->employee = $employee;
        $this->employeeType = $employeeType;
        $this->tempStore = $tempStore->get('employee_salary_session');
    }

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public static function create(ContainerInterface $container)
    {
        return new static($container->get('request_stack'), $container->get('employee_salary.work_time'), $container->get('employee_salary.employee'), $container->get('employee_salary.employee_type'), $container->get('user.private_tempstore'));
    }

    public function listWorkers()
    {
        $header = $this->workTime->getHeaders()['listWorkers'];
        $employeesCheckedIn = $this->workTime->getActiveWorkers();
        
        // The table description.
        $build['table'] = [
            '#type' => 'table',
            '#header' => $header,
            '#rows' => [],
            '#empty' => $this->t('No hay emplados ingresados.')
        ];
        
        foreach ($employeesCheckedIn as $employeeChekedIn) {
            
            $row['id']['data'] = $employeeChekedIn->getId();
            
            $this->employee->setId($employeeChekedIn->getEmployee());
            $this->employee->query();
            
            $row['employee']['data'] = $this->employee->getName() . ' ' . $this->employee->getLastName();
            $row['date']['data'] = $employeeChekedIn->getDate();
            
            $ingressFormated = new DateTime();
            $ingressFormated->setTimestamp($employeeChekedIn->getIngressTime());
            
            $row['ingressTime']['data'] = $ingressFormated->format('Y/m/d H:i');
            
            $urlClose = Url::fromRoute('employee_salary.worktime_finish', [
                'id' => $employeeChekedIn->getId()
            ]);
            
            $row['close']['data'] = Link::fromTextAndUrl('Cerrar', $urlClose);
            
            $build['table']['#rows'][$employeeChekedIn->getId()] = $row;
        }
        
        $build['pager'] = [
            '#type' => 'pager',
            '#quantity' => 10
        ];
        
        $build['#cache'] = [
            'disabled' => TRUE
        ];
        
        return $build;
    }

    public function hourBehavior()
    {
        $workBehaviorFormName = 'Drupal\employee_salary\Form\WorkBehaviorForm';
        $workBehaviorForm = \Drupal::formBuilder()->getForm($workBehaviorFormName);
        
        $header = $this->workTime->getHeaders()['behavior'];
        $rows = [];
        
        if ($this->tempStore->get('workBehaviorEmployee') && $this->tempStore->get('workBehaviorStartDate') && $this->tempStore->get('workBehaviorEndDate')) {
            $employeesCheckedIn = $this->workTime->getTotalTimeWorked($this->tempStore->get('workBehaviorEmployee'), $this->tempStore->get('workBehaviorStartDate'), $this->tempStore->get('workBehaviorEndDate'));
            
            foreach ($employeesCheckedIn as $employeeChekedIn) {
                
                $row['id']['data'] = $employeeChekedIn->getId();
                
                $this->employee->setId($employeeChekedIn->getEmployee());
                $this->employee->query();
                
                $ingressFormated = new DateTime();
                $ingressFormated->setTimestamp($employeeChekedIn->getIngressTime());
                $exitFormated = new DateTime();
                $exitFormated->setTimestamp($employeeChekedIn->getExitTime());
                
                $row['day']['data'] = $ingressFormated->format('l');
                
                $row['ingressTime']['data'] = $ingressFormated->format('Y/m/d H:i');
                $row['exitTime']['data'] = $exitFormated->format('Y/m/d H:i');
                
                $rows[$employeeChekedIn->getId()] = $row;
            }
            $this->tempStore->delete('workBehaviorEmployee');
            $this->tempStore->delete('workBehaviorStartDate');
            $this->tempStore->delete('workBehaviorEndDate');
        }
        
        $build = [
            'form' => $workBehaviorForm,
            'table' => [
                '#type' => 'table',
                '#header' => $header,
                '#rows' => $rows,
                '#empty' => $this->t('No hay información.')
            ],
            'pager' => [
                '#type' => 'pager',
                '#quantity' => 10
            ]
        ];
        
        return $build;
    }

    public function finish($id)
    {
        
        $finished = $this->workTime->finish($id);
        
        if($finished){
            drupal_set_message('Worker Checked out');
        } else {
            drupal_set_message("Worker don't Checked out", 'error');
        }
        
        $urlRedirect = Url::fromRoute('employee_salary.worktime_list');
        
        return new RedirectResponse($urlRedirect->toString());
    }
}


<?php

namespace Drupal\employee_salary\Controller;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\employee_salary\Model\Loan;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\employee_salary\Model\Employee;

/**
 * Class LoanController.
 */
class LoanController extends ControllerBase {

  /**
     * Symfony\Component\HttpFoundation\RequestStack definition.
     *
     * @var object
     */
    protected $requestStack;

    /**
     * Drupal\user\PrivateTempStoredefinition.
     *
     * @var Drupal\user\PrivateTempStore
     */
    protected $tempStore;
    
    /**
     * Drupal\employee_salary\Model\Loan
     *
     * @var object
     */
    protected $loan;
    
    /**
     * Drupal\employee_salary\Model\Employee
     *
     * @var object
     */
    protected $employee;

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public function __construct(RequestStack $requestStack, PrivateTempStoreFactory $tempStore, Loan $loan, Employee $employee)
    {
        $this->requestStack = $requestStack;
        $this->tempStore = $tempStore->get('tigoune_scheduling_session');
        $this->loan = $loan;
        $this->employee = $employee;
    }

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public static function create(ContainerInterface $container)
    {
        return new static($container->get('request_stack'), $container->get('user.private_tempstore'), $container->get('employee_salary.loan'), $container->get('employee_salary.employee'));
    }
    
    public function listLoans()
    {
        $header = $this->loan->getHeaders();
        $loans = $this->loan->listLoans();
        
        // The table description.
        $build['table'] = [
            '#type' => 'table',
            '#header' => $header,
            '#rows' => [],
            '#empty' => $this->t('There is no yet.')
        ];
        
        foreach ($loans as $loan) {
            
            $row['id']['data'] = $loan->id;
            
            $this->employee->setId($loan->employee);
            $this->employee->query();
            
            $row['employee']['data'] = $this->employee->getName() . ' ' . $this->employee->getLastName();
            $row['description']['data'] = $loan->description;
            $row['totalAmount']['data'] = $loan->totalAmount;
            $row['currentAmount']['data'] = $loan->currentAmount;
            
            $urlEdit = Url::fromRoute('employee_salary.loan_edit', [
                'id' => $loan->id
            ]);
            $urlDelete = Url::fromRoute('employee_salary.loan_delete', [
                'id' => $loan->id
            ]);
            $row['edit']['data'] = Link::fromTextAndUrl('Edit', $urlEdit);
            $row['delete']['data'] = Link::fromTextAndUrl('Delete', $urlDelete);
            
            $build['table']['#rows'][$loan->id] = $row;
        }
        
        $build['pager'] = [
            '#type' => 'pager',
            '#quantity' => 10
        ];
        
        return $build;
    }
    
    public function deleteLoan($id){
        
        $this->loan->setId($id);
        $this->loan->query();
        
        if($this->loan->getId()){
            $this->loan->delete();
        } else {
            drupal_set_message('Loan Not Exists');
        }
        
        return RedirectResponse::create('/loan');
        
    }
    

}

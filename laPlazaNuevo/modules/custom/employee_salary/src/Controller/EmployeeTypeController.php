<?php
namespace Drupal\employee_salary\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\employee_salary\Model\EmployeeType;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class EmployeeController.
 */
class EmployeeTypeController extends ControllerBase
{

    /**
     * Symfony\Component\HttpFoundation\RequestStack definition.
     *
     * @var object
     */
    protected $requestStack;

    /**
     * Drupal\user\PrivateTempStoredefinition.
     *
     * @var Drupal\user\PrivateTempStore
     */
    protected $tempStore;

    /**
     * Drupal\employee_salary\Model\EmployeeType
     *
     * @var object
     */
    protected $employeeType;

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public function __construct(RequestStack $requestStack, PrivateTempStoreFactory $tempStore, EmployeeType $employeeType)
    {
        $this->requestStack = $requestStack;
        $this->tempStore = $tempStore->get('tigoune_scheduling_session');
        $this->employeeType = $employeeType;
    }

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public static function create(ContainerInterface $container)
    {
        return new static($container->get('request_stack'), $container->get('user.private_tempstore'), $container->get('employee_salary.employee_type'));
    }

    public function listEmployeeTypes()
    {
        $header = $this->employeeType->getHeaders();
        $employeeTypes = $this->employeeType->listEmployType();
        
        // The table description.
        $build['table'] = [
            '#type' => 'table',
            '#header' => $header,
            '#rows' => [],
            '#empty' => $this->t('There is no yet.'),
        ];
        
        foreach ($employeeTypes as $employeeType) {
            
            $row['id']['data'] = $employeeType->id;
            $row['name']['data'] = $employeeType->name;
            $row['description']['data'] = $employeeType->description;
            
            $urlEdit = Url::fromRoute('employee_salary.employee_type_edit',['id' => $employeeType->id]);
            $urlDelete = Url::fromRoute('employee_salary.employee_type_delete',['id' => $employeeType->id]);
            //$row['view']['data'] = Link::fromTextAndUrl('View', $urlEdit);
            $row['edit']['data'] = Link::fromTextAndUrl('Edit', $urlEdit);
            $row['delete']['data'] = Link::fromTextAndUrl('Delete', $urlDelete);
            
            $build['table']['#rows'][$employeeType->id] = $row;

        }
        
        $build['pager'] = [
            '#type' => 'pager',
            '#quantity' => 10
        ];
        
        return $build;
        
    }
    
    public function ConfirmDeleteEmployeeType($id){
        
        kint('edit',$id);
        die;
        
    }
    
    public function deleteEmployeeType($id){
        
        $this->employeeType->setId($id);
        $this->employeeType->query();
        
        if($this->employeeType->getId()){
            $this->employeeType->delete();
        } else {
            drupal_set_message('Employee Type Not Exists');
        }
        
        return RedirectResponse::create('/EmployeeType');
               
    }
    
}

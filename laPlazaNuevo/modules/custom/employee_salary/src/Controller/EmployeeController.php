<?php
namespace Drupal\employee_salary\Controller;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\employee_salary\Model\Employee;
use Drupal\employee_salary\Model\EmployeeType;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class EmployeeController extends ControllerBase
{

    /**
     * Symfony\Component\HttpFoundation\RequestStack definition.
     *
     * @var object
     */
    protected $requestStack;

    /**
     * Drupal\user\PrivateTempStoredefinition.
     *
     * @var Drupal\user\PrivateTempStore
     */
    protected $tempStore;

    /**
     * Drupal\employee_salary\Model\Employee
     *
     * @var object
     */
    protected $employee;

    /**
     * Drupal\employee_salary\Model\EmployeeType
     *
     * @var object
     */
    protected $employeeType;

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public function __construct(RequestStack $requestStack, PrivateTempStoreFactory $tempStore, Employee $employee, EmployeeType $employeeType)
    {
        $this->requestStack = $requestStack;
        $this->tempStore = $tempStore->get('tigoune_scheduling_session');
        $this->employee = $employee;
        $this->employeeType = $employeeType;
    }

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public static function create(ContainerInterface $container)
    {
        return new static($container->get('request_stack'), $container->get('user.private_tempstore'), $container->get('employee_salary.employee'), $container->get('employee_salary.employee_type'));
    }

    public function listEmployees()
    {
        $header = $this->employee->getHeaders();
        $employees = $this->employee->listEmployees();
        
        // The table description.
        $build['table'] = [
            '#type' => 'table',
            '#header' => $header,
            '#rows' => [],
            '#empty' => $this->t('There is no yet.')
        ];
        
        foreach ($employees as $employee) {
            
            $row['id']['data'] = $employee->id;
            
            $this->employeeType->setId($employee->employeeType);
            $this->employeeType->query();
            
            $row['name']['data'] = $employee->name . ' ' . $employee->lastName;
            $row['type']['data'] = $this->employeeType->getName();
            $row['phone']['data'] = $employee->phone;
            
            $urlEdit = Url::fromRoute('employee_salary.employee_edit', [
                'id' => $employee->id
            ]);
            $urlDelete = Url::fromRoute('employee_salary.employee_delete', [
                'id' => $employee->id
            ]);
            $row['edit']['data'] = Link::fromTextAndUrl('Edit', $urlEdit);
            $row['delete']['data'] = Link::fromTextAndUrl('Delete', $urlDelete);
            
            $build['table']['#rows'][$employee->id] = $row;
        }
        
        $build['pager'] = [
            '#type' => 'pager',
            '#quantity' => 10
        ];
        
        return $build;
    }

    public function deleteEmployee($id){
        
        $this->employee->setId($id);
        $this->employee->query();
        
        if($this->employee->getId()){
            $this->employee->delete();
        } else {
            drupal_set_message('Employee Not Exists');
        }
        
        return RedirectResponse::create('/Employee');
        
    }
}


<?php
namespace Drupal\employee_salary\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\employee_salary\Model\Salary;
use Drupal\employee_salary\Model\WorkTime;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;

class TestController extends ControllerBase
{

    public function printTest()
    {
        try {
            // Enter connector and capability profile (to match your printer)
            $connector = new FilePrintConnector("LPT3");
            
            /* Print a series of receipts containing i18n example strings */
            $printer = new Printer($connector);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_WIDTH);
            $printer->text("   LIQUIDACIÓN\n");
            $printer->selectPrintMode();
            
            $printer->feed();

            $printer->setEmphasis(true);
            $printer->text("Tipo Liqudación: ");
            $printer->setEmphasis(false);
            $printer->text("Horas\n");
            
            $printer->setEmphasis(true);
            $printer->text("Nombre:\n");
            $printer->setEmphasis(false);
            $printer->text("David Alberto Zuluaga Rios\n");
            
            
            $printer->setEmphasis(true);
            $printer->text("Periodo Liquidado:\n");
            $printer->setEmphasis(false);
            $printer->text("2018/04/01 - 2018/04/15\n");
            
            $printer->feed();
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $printer->text("Ingresos\n");
            $printer->selectPrintMode();
            
            $printer->setEmphasis(true);
            $printer->text("Salario:  ");
            $printer->setEmphasis(false);
            $printer->text("$ 0\n");
            
            $printer->setEmphasis(true);
            $printer->text("Horas Extras:  ");
            $printer->setEmphasis(false);
            $printer->text("$ 552,379\n");
            
            $printer->setEmphasis(true);
            $printer->text("Sub. Transporte:  ");
            $printer->setEmphasis(false);
            $printer->text("$ 35,284\n");
            
            $printer->feed();
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $printer->text("Deducciones\n");
            $printer->selectPrintMode();
            
            $printer->setEmphasis(true);
            $printer->text("Seguridad Social:  ");
            $printer->setEmphasis(false);
            $printer->text("$ 22,095\n");
            
            $printer->setEmphasis(true);
            $printer->text("Prestamos:  ");
            $printer->setEmphasis(false);
            $printer->text("$ 0\n");
          
            $printer->feed();
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_EMPHASIZED);
            $printer->text("Total:         ");            
            $printer->text("$ 565,568\n");
            $printer->selectPrintMode();
            
            $printer->feed(2);
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $printer->text("Detalle Horas\n");
            $printer->selectPrintMode();
            
            $printer->setEmphasis(true);
            $printer->text("Diurnas:  ");
            $printer->setEmphasis(false);
            $printer->text("99:30\n");
            
            $printer->setEmphasis(true);
            $printer->text("Nocturnas:  ");
            $printer->setEmphasis(false);
            $printer->text("36:30\n");
            
            $printer->setEmphasis(true);
            $printer->text("Diurnas Festivas:  ");
            $printer->setEmphasis(false);
            $printer->text("0:0\n");
            
            $printer->setEmphasis(true);
            $printer->text("Nocturnas Festivas:  ");
            $printer->setEmphasis(false);
            $printer->text("4:22\n");
            
            $printer->feed();
            
            $printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $printer->text("Detalle Prestamos\n");
            $printer->selectPrintMode();
            
            $printer->setEmphasis(true);
            $printer->text("Prestamo 1:  ");
            $printer->setEmphasis(false);
            $printer->text("$ 100,000\n");
            
            $printer->setEmphasis(true);
            $printer->text("Prestamo 2:  ");
            $printer->setEmphasis(false);
            $printer->text("$ 100,000\n");
            
            $printer->setEmphasis(true);
            $printer->text("Prestamo 3:  ");
            $printer->setEmphasis(false);
            $printer->text("$ 100,000\n");
            
            
            $printer->cut();
            /* Close printer */
            $printer->close();
        } catch (\Exception $e) {
            echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
        }
    }
}


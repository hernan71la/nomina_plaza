<?php
namespace Drupal\employee_salary\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\employee_salary\Model\Employee;
use Drupal\employee_salary\Model\EmployeeType;
use Drupal\employee_salary\Model\Salary;
use Drupal\employee_salary\Model\WorkTime;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\employee_salary\Form\SalaryActionsForm;

class SalaryController extends ControllerBase
{

    /**
     *
     * @var \Drupal\employee_salary\Model\WorkTime
     */
    protected $workTime;

    /**
     *
     * @var \Drupal\employee_salary\Model\Employee
     */
    protected $employee;

    /**
     *
     * @var \Drupal\employee_salary\Model\EmployeeType
     */
    protected $employeeType;

    /**
     * Symfony\Component\HttpFoundation\RequestStack definition.
     *
     * @var object
     */
    protected $requestStack;

    /**
     * Drupal\user\PrivateTempStoredefinition.
     *
     * @var Drupal\user\PrivateTempStore
     */
    protected $tempStore;

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public function __construct(RequestStack $requestStack, WorkTime $workTime, Employee $employee, EmployeeType $employeeType, PrivateTempStoreFactory $tempStore)
    {
        $this->requestStack = $requestStack;
        $this->workTime = $workTime;
        $this->employee = $employee;
        $this->employeeType = $employeeType;
        $this->tempStore = $tempStore->get('employee_salary_session');
    }

    /**
     * {@inheritdoc<mailto:{@inheritdoc>}
     */
    public static function create(ContainerInterface $container)
    {
        return new static($container->get('request_stack'), $container->get('employee_salary.work_time'), $container->get('employee_salary.employee'), $container->get('employee_salary.employee_type'), $container->get('user.private_tempstore'));
    }

    public function viewSalaryHistory()
    {
        $employee = $this->requestStack->getCurrentRequest()->get('salaryEmployee');
        $startDate = $this->requestStack->getCurrentRequest()->get('salaryStartDate');
        $endDate = $this->requestStack->getCurrentRequest()->get('salaryEndDate');
        
        $employeeDetails = new Employee();
        $employeeDetails->setId($employee);
        $employeeDetails->query();
        
        $employeeType = new EmployeeType();
        $employeeType->setId($employeeDetails->getType());
        $employeeType->query();
        
        
        $workTime = new WorkTime();
        $salary = new Salary($employee, $startDate, $endDate);
        
        $salary->calculateSalaryBase();
        $salary->setWorkedTimes($workTime->getTotalTimeWorked($employee, $startDate, $endDate));
        $salary->calculateTotalWorkedAmount();
        $salary->calculateTransportationSubsidy();
        $salary->calculateSecurity();
        $salary->calculateTotalAmount();
        
        $extraHoursDetails = $salary->getDetailsExtraHours();
        
        $discounts = $salary->getLoans();
        
        
        $totalDiscount = 0;
        foreach ($discounts as $discount){
            $totalDiscount = $totalDiscount + $discount['value'];
        }
        
        $salaryId =  $salary->save();
        
        $salary->saveDetails($salaryId, $extraHoursDetails);
        
        $form = new SalaryActionsForm(\Drupal::service('employee_salary.printer'));
        $form->setSalaryInfo($employeeDetails, $employeeType, $salary, $extraHoursDetails, $totalDiscount, $discounts);
        
        $salaryActionsForm = \Drupal::formBuilder()->getForm($form);
        $build = [
            '#theme' => 'employee_salary_salary',
            '#employeeName' => $employeeDetails->getName() . ' ' . $employeeDetails->getLastName(),
            '#employeeType' => $employeeType->getName(),
            '#initialDate' => $salary->getInitialDate(),
            '#endDate' => $salary->getFinalDate(),
            '#salaryAmount' => $salary->getSalaryAmount(),
            '#workedAmount' => $salary->getTotalWorkedAmount(),
            '#securityAmount' => $salary->getSecurityValue(),
            '#transportationAmount' => $salary->getTransportationValue(),
            '#totalAmount' => $salary->getTotalAmount() - $totalDiscount,
            '#diurnalTime' => $extraHoursDetails['diurnalTime'],
            '#nocturnalTime' => $extraHoursDetails['nocturnalTime'],
            '#holyDiurnalTime' => $extraHoursDetails['holyDiurnalTime'],
            '#holyNocturnalTime' => $extraHoursDetails['holyNocturnalTime'],
            '#totalDiscount' => $totalDiscount,
            '#discounts' => $discounts,
            '#form' => $salaryActionsForm
        ];
        
        return $build;
        
    }
}


<?php
namespace Drupal\employee_salary\Model;

use DateTime;
use Drupal\employee_salary\Util\Date;
use Drupal\employee_salary\Util\Festivos;

class WorkTime
{

    const TABLE = 'employee_salary_work_time';

    const ALIAS = 'wt';

    private $id;

    private $employee;

    private $date;

    private $ingressTime;

    private $exitTime;

    private $daytimeHours;

    private $daytimeMinutes;

    private $daytimeGain;

    private $nightlyHours;

    private $nightlyMinutes;

    private $nightlyGain;

    private $holydayDaytimeHours;

    private $holydayDaytimeMinutes;

    private $holydayDaytimeGain;

    private $holydayNightlyHours;

    private $holydayNightlyMinutes;

    private $holydayNightlyGain;

    public function __construct()
    {
    }

    public function save()
    {
        $database = \Drupal::database();
        return $database->insert(WorkTime::TABLE)
            ->fields([
            'employee' => $this->getEmployee(),
            'date' => date('Y-m-d'),
            'ingressTime' => time(),
            'createdBy' => ''
        
        ])
            ->execute();
    }

    public function load($id)
    {
        $database = \Drupal::database();
        
        $query = $database->select(WorkTime::TABLE, WorkTime::ALIAS)->fields(WorkTime::ALIAS, [
            'id',
            'employee',
            'date',
            'ingressTime',
            'exitTime'
        ]);
        $andCondition = $query->andConditionGroup();
        
        $andCondition->condition('id', $id);
        $andCondition->isNull('exitTime');
        
        $query->condition($andCondition);
        
        $result = $query->execute()->fetchAll(\PDO::FETCH_OBJ);
        
        $this->employee = $result[0]->employee;
        $this->setIngressTime($result[0]->ingressTime);
        $this->setExitTime($result[0]->exitTime);
    }
    
    public function finish($id)
    {
        $this->load($id);
        
        $this->setExitTime(time());
        
        $this->calculateTimes();
        
        $this->calculateWorkTimeRequired();
        
        $this->calculateGains();
        
        $database = \Drupal::database();
        
        $update = $database->update(WorkTime::TABLE)->fields([
            'exitTime' => $this->getExitTime(),
            'daytimeHours' => $this->daytimeHours,
            'daytimeMinutes' => $this->daytimeMinutes,
            'nightlyHours' => $this->nightlyHours,
            'nightlyMinutes' => $this->nightlyMinutes,
            'holydayDaytimeHours' => $this->holydayDaytimeHours,
            'holydayDaytimeMinutes' => $this->holydayDaytimeMinutes,
            'holydayNightlyHours' => $this->holydayNightlyHours,
            'holydayNightlyMinutes' => $this->holydayNightlyMinutes,
            'daytimeGain' => $this->daytimeGain,
            'nightlyGain' => $this->nightlyGain,
            'holydayDaytimeGain' => $this->holydayDaytimeGain,
            'holydayNightlyGain' => $this->holydayNightlyGain
        ]);
        $andCondition = $update->andConditionGroup();
        
        $andCondition->condition('id', $id);
        $andCondition->isNull('exitTime');
        
        return $update->condition($andCondition)->execute();
    }

    public static function active()
    {
        $database = \Drupal::database();
        $query = $database->select(WorkTime::TABLE, WorkTime::ALIAS)->fields(WorkTime::ALIAS, [
            'employee'
        ]);
        $query->isNull('exitTime');
        $query->groupBy('employee');
        $query->orderBy('id', 'DESC');
        return $query->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function getActiveWorkers()
    {
        $database = \Drupal::database();
        $query = $database->select(WorkTime::TABLE, WorkTime::ALIAS)->fields(WorkTime::ALIAS, [
            'id',
            'employee',
            'date',
            'ingressTime'
        ]);
        $query->isNull('exitTime');
        $query->orderBy('id', 'DESC');
        
        return $query->execute()->fetchAll(\PDO::FETCH_CLASS, 'Drupal\employee_salary\Model\WorkTime');
    }

    public function getHeaders()
    {
        $headers = [
            'listWorkers' => [
                'id' => 'Id',
                'employee' => 'Nombre',
                'date' => 'Fecha',
                'ingressTime' => 'Ingreso',
                'close' => ''
            ],
            'behavior' => [
                'id' => 'Id',
                'day' => 'Día',
                'ingressTime' => 'Ingreso',
                'exitTime' => 'Salida'
            ]
        ];
        
        return $headers;
    }

    public function getTotalTimeWorked($employee, $startDate, $endDate)
    {
        $database = \Drupal::database();
        $query = $database->select(WorkTime::TABLE, WorkTime::ALIAS)->fields(WorkTime::ALIAS, [
            'id',
            'employee',
            'date',
            'ingressTime',
            'exitTime',
            'daytimeHours',
            'daytimeMinutes',
            'daytimeGain',
            'nightlyHours',
            'nightlyMinutes',
            'nightlyGain',
            'holydayDaytimeHours',
            'holydayDaytimeMinutes',
            'holydayDaytimeGain',
            'holydayNightlyHours',
            'holydayNightlyMinutes',
            'holydayNightlyGain'
        ]);
        $andCondition = $query->andConditionGroup();
        $andCondition->isNotNull('exitTime');
        $andCondition->condition('employee', $employee);
        $andCondition->condition('date', [
            $startDate,
            $endDate
        ], 'BETWEEN');
        $query->condition($andCondition);
        $data = $query->execute()->fetchAll(\PDO::FETCH_CLASS, 'Drupal\employee_salary\Model\WorkTime');
        
        return $data;
    }

    public function calculateTimes()
    {
        $startTime = new \DateTime();
        $startTime->setTimestamp($this->ingressTime);
        
        $endTime = new \DateTime();
        $endTime->setTimestamp($this->exitTime);        
        
        $endTime = $this->cleanTime($startTime, $endTime);
        $this->exitTime = $endTime->getTimestamp();
        
        if ($startTime < $endTime) {
            
            $diurnal = Date::calculateDiurnalHours($startTime, $endTime);
            $nocturnal = Date::calculateNocturnalHours($startTime, $endTime);
            $times = array_merge($diurnal, $nocturnal);
            
            $this->daytimeHours += $times['diurnalHours'];
            $this->daytimeMinutes += $times['diurnalMinutes'];
            $this->nightlyHours += $times['nocturnalHours'];
            $this->nightlyMinutes += $times['nocturnalMinutes'];
            $this->holydayDaytimeHours += $times['holydayDiurnalHours'];
            $this->holydayDaytimeMinutes += $times['holydayDiurnalMinutes'];
            $this->holydayNightlyHours += $times['holydayNocturnalHours'];
            $this->holydayNightlyMinutes += $times['holydayNocturnalMinutes'];
        }
    }

    private function cleanTime($startTime, $endTime)
    {
        $config = \Drupal::config('employee_salary.config');
        
        $limitExitHour = clone $startTime;
        $limitExitHour->setTime($config->get('maxExitHour'), 0);
        
        if ($startTime <= $limitExitHour) {
            if ($endTime > $limitExitHour) {
                $endTime->setTime($config->get('maxExitHour'), 0);
            }
        } else {
            $limitExitHour->add(new \DateInterval('P1D'));
            if ($endTime > $limitExitHour) {
                $endTime = clone $limitExitHour;
            }
        }
        
        return $endTime;
    }

    private function calculateWorkTimeRequired()
    {
        $employee = new Employee();
        $employee->setId($this->employee);
        $employee->query();
        
        $employeeType = new EmployeeType();
        $employeeType->setId($employee->getType());
        $employeeType->query();
        
        $minutesRequired = $employeeType->getNumberWorkHours() * 60;
        $minutesWorkedDaytime = $this->daytimeHours * 60 + $this->daytimeMinutes;
        $minutesWorkedNightly = $this->nightlyHours * 60 + $this->nightlyMinutes;
        
        if ($minutesRequired <= $minutesWorkedDaytime) {
            $minutesWorkedDaytime -= $minutesRequired;
        } else {
            $minutesRequired -= $minutesWorkedDaytime;
            $minutesWorkedDaytime = 0;
            
            if ($minutesRequired <= $minutesWorkedNightly) {
                $minutesWorkedNightly -= $minutesRequired;
            } else {
                $minutesWorkedNightly = 0;
            }
        }
        
        $this->daytimeHours = floor($minutesWorkedDaytime / 60);
        $this->daytimeMinutes = $minutesWorkedDaytime % 60;
        
        $this->nightlyHours = floor($minutesWorkedNightly / 60);
        $this->nightlyMinutes = $minutesWorkedNightly % 60;
    }

    private function calculateGains()
    {
        $config = \Drupal::config('employee_salary.config');
        
        $employee = new Employee();
        $employee->setId($this->employee);
        $employee->query();
        
        $employeeType = new EmployeeType();
        $employeeType->setId($employee->getType());
        $employeeType->query();
        
        $this->daytimeGain = $this->daytimeHours * ($employeeType->getHourValue() * $config->get('diurnalNormal'));
        $this->daytimeGain += $this->daytimeMinutes * floor((($employeeType->getHourValue() * $config->get('diurnalNormal')) / 60));
        $this->nightlyGain = $this->nightlyHours * ($employeeType->getHourValue() * $config->get('nocturnalNormal'));
        $this->nightlyGain += $this->nightlyMinutes * floor((($employeeType->getHourValue() * $config->get('nocturnalNormal')) / 60));
        $this->holydayDaytimeGain = $this->holydayDaytimeHours * ($employeeType->getHourValue() * $config->get('diurnalHoliday'));
        $this->holydayDaytimeGain += $this->holydayDaytimeMinutes * floor((($employeeType->getHourValue() * $config->get('diurnalHoliday')) / 60));
        $this->holydayNightlyGain = $this->holydayNightlyHours * ($employeeType->getHourValue() * $config->get('nocturnalHoliday'));
        $this->holydayNightlyGain += $this->holydayNightlyMinutes * floor((($employeeType->getHourValue() * $config->get('nocturnalHoliday')) / 60));
    }

    /**
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     *
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     *
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     *
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     *
     * @return mixed
     */
    public function getIngressTime()
    {
        return $this->ingressTime;
    }

    /**
     *
     * @param mixed $ingressTime
     */
    public function setIngressTime($ingressTime)
    {
        $this->ingressTime = $ingressTime;
    }

    /**
     *
     * @return mixed
     */
    public function getExitTime()
    {
        return $this->exitTime;
    }

    /**
     *
     * @param mixed $exitTime
     */
    public function setExitTime($exitTime)
    {
        $this->exitTime = $exitTime;
    }
    /**
     * @return number
     */
    public function getDaytimeGain()
    {
        return $this->daytimeGain;
    }

    /**
     * @param number $daytimeGain
     */
    public function setDaytimeGain($daytimeGain)
    {
        $this->daytimeGain = $daytimeGain;
    }

    /**
     * @return number
     */
    public function getNightlyGain()
    {
        return $this->nightlyGain;
    }

    /**
     * @param number $nightlyGain
     */
    public function setNightlyGain($nightlyGain)
    {
        $this->nightlyGain = $nightlyGain;
    }

    /**
     * @return number
     */
    public function getHolydayDaytimeGain()
    {
        return $this->holydayDaytimeGain;
    }

    /**
     * @param number $holydayDaytimeGain
     */
    public function setHolydayDaytimeGain($holydayDaytimeGain)
    {
        $this->holydayDaytimeGain = $holydayDaytimeGain;
    }

    /**
     * @return number
     */
    public function getHolydayNightlyGain()
    {
        return $this->holydayNightlyGain;
    }

    /**
     * @param number $holydayNightlyGain
     */
    public function setHolydayNightlyGain($holydayNightlyGain)
    {
        $this->holydayNightlyGain = $holydayNightlyGain;
    }
    /**
     * @return mixed
     */
    public function getDaytimeHours()
    {
        return $this->daytimeHours;
    }

    /**
     * @param mixed $daytimeHours
     */
    public function setDaytimeHours($daytimeHours)
    {
        $this->daytimeHours = $daytimeHours;
    }

    /**
     * @return Ambigous <number, mixed>
     */
    public function getDaytimeMinutes()
    {
        return $this->daytimeMinutes;
    }

    /**
     * @param Ambigous <number, mixed> $daytimeMinutes
     */
    public function setDaytimeMinutes($daytimeMinutes)
    {
        $this->daytimeMinutes = $daytimeMinutes;
    }

    /**
     * @return mixed
     */
    public function getNightlyHours()
    {
        return $this->nightlyHours;
    }

    /**
     * @param mixed $nightlyHours
     */
    public function setNightlyHours($nightlyHours)
    {
        $this->nightlyHours = $nightlyHours;
    }

    /**
     * @return Ambigous <number, mixed>
     */
    public function getNightlyMinutes()
    {
        return $this->nightlyMinutes;
    }

    /**
     * @param Ambigous <number, mixed> $nightlyMinutes
     */
    public function setNightlyMinutes($nightlyMinutes)
    {
        $this->nightlyMinutes = $nightlyMinutes;
    }

    /**
     * @return mixed
     */
    public function getHolydayDaytimeHours()
    {
        return $this->holydayDaytimeHours;
    }

    /**
     * @param mixed $holydayDaytimeHours
     */
    public function setHolydayDaytimeHours($holydayDaytimeHours)
    {
        $this->holydayDaytimeHours = $holydayDaytimeHours;
    }

    /**
     * @return mixed
     */
    public function getHolydayDaytimeMinutes()
    {
        return $this->holydayDaytimeMinutes;
    }

    /**
     * @param mixed $holydayDaytimeMinutes
     */
    public function setHolydayDaytimeMinutes($holydayDaytimeMinutes)
    {
        $this->holydayDaytimeMinutes = $holydayDaytimeMinutes;
    }

    /**
     * @return mixed
     */
    public function getHolydayNightlyHours()
    {
        return $this->holydayNightlyHours;
    }

    /**
     * @param mixed $holydayNightlyHours
     */
    public function setHolydayNightlyHours($holydayNightlyHours)
    {
        $this->holydayNightlyHours = $holydayNightlyHours;
    }

    /**
     * @return mixed
     */
    public function getHolydayNightlyMinutes()
    {
        return $this->holydayNightlyMinutes;
    }

    /**
     * @param mixed $holydayNightlyMinutes
     */
    public function setHolydayNightlyMinutes($holydayNightlyMinutes)
    {
        $this->holydayNightlyMinutes = $holydayNightlyMinutes;
    }

}


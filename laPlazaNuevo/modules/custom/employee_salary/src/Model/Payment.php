<?php
namespace Drupal\employee_salary\Model;

class Payment
{

    const TABLE = 'employee_salary_payment';

    const ALIAS = 'pa';

    private $id;

    private $loan;

    private $amount;
    
    private $type;

    public function validate()
    {
        $fieldsFail = [];
        
        /*
         * TODO: Write validation error messages.
         * TODO: refine the validations
         */
        
        if (is_null($this->getLoan()) || empty($this->getLoan())) {
            array_push($fieldsFail, [
                'name' => 'loan',
                'message' => 'Debe proporcionar un prestamo'
            ]);
        }
        
        if (is_null($this->getAmount()) || empty($this->getAmount())) {
            array_push($fieldsFail, [
                'name' => 'amount',
                'message' => 'Debe proporcionar un monto'
            ]);
        }
        
        return $fieldsFail;
    }

    public function save()
    {
        
        /*
         * TODO: get Current User na save it
         */
        $database = \Drupal::database();
        return $database->insert(Payment::TABLE)
            ->fields([
            'loan' => $this->getLoan(),
            'amount' => $this->getAmount(),
            'createdAt' => time(),
            'createdBy' => '',
            'typePayment' => $this->getType()
        
        ])
            ->execute();
    }
    
    public function getLastPaymentOfLoan($loanId){
        
        $database = \Drupal::database();
        $data = $database->select(Payment::TABLE, Payment::ALIAS)
        ->fields(Payment::ALIAS, [
            'id',
            'amount',
            'createdAt'
        ]);
        
        $data->condition('loan', $loanId);
        $data->condition('typePayment', 'discount');
        $data->orderBy('id', 'DESC');
        
        return $data->execute()->fetchAll(\PDO::FETCH_CLASS, 'Drupal\employee_salary\Model\Payment');
    }

    /**
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @return mixed
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     *
     * @param mixed $loan
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;
    }

    /**
     *
     * @return mixed
     */
    public function getAmount() 
    {
        return $this->amount;
    }

    /**
     *
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
    
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    
}


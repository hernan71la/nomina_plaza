<?php
namespace Drupal\employee_salary\Model;

class Salary
{

    const TABLE = 'employee_salary_salary_history';

    const TABLE_DETAILS = 'employee_salary_salary_history_details';

    const ALIAS = 'sh';

    private $id;

    private $employee;

    private $initialDate;

    private $finalDate;

    private $salaryAmount;

    private $totalWorkedAmount;

    private $totalAmount;

    private $securityValue;

    private $transportationValue;

    private $salaryDetails;

    private $workedTimes;

    public function __construct($employee, $initialDate, $finalDate)
    {
        $this->employee = $employee;
        $this->initialDate = $initialDate;
        $this->finalDate = $finalDate;
    }

    public function setWorkedTimes($workedTimes)
    {
        $this->workedTimes = $workedTimes;
    }

    public function save()
    {
        $database = \Drupal::database();
        return $database->insert(Salary::TABLE)
            ->fields([
            'employee' => $this->getEmployee(),
            'startDate' => $this->getInitialDate(),
            'endDate' => $this->getFinalDate(),
            'workedValue' => $this->getTotalWorkedAmount(),
            'securityValue' => $this->getSecurityValue(),
            'transportationValue' => $this->getTransportationValue(),
            'totalValue' => $this->getTotalAmount(),
            'salaryBase' => $this->getSalaryAmount(),
            'createdAt' => time()
        
        ])
            ->execute();
    }

    public function saveDetails($salaryId, $details)
    {
        foreach ($details as $detail => $value) {
            $database = \Drupal::database();
            $database->insert(Salary::TABLE_DETAILS)
                ->fields([
                'salaryHistory' => $salaryId,
                'date' => time(),
                'concept' => $detail,
                'value' => $value
            ])
                ->execute();
        }
    }

    public function validateSalaryGeneration()
    {
        $database = \Drupal::database();
        
        $query = $database->select(Salary::TABLE, Salary::ALIAS)->fields(Salary::ALIAS, [
            'id'
        ]);
        
        $andCondition = $query->andConditionGroup();
        $andCondition->condition('employee', $this->getEmployee());
        $andCondition->condition('startDate', $this->getInitialDate());
        $andCondition->condition('endDate', $this->getFinalDate());
        
        $query->condition($andCondition);
        
        $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
        
        return $result;
    }

    public function validateNumberOfDays()
    {
        $dateDiff = $this->initialDate->diff($this->finalDate);
        if ($dateDiff->d < 30) {
            return true;
        } else {
            return false;
        }
    }

    public function calculateTotalWorkedAmount()
    {
        $employee = new Employee();
        $employee->setId($this->employee);
        $employee->query();
        
        $employeeType = new EmployeeType();
        $employeeType->setId($employee->getType());
        $employeeType->query();
        
        foreach ($this->workedTimes as $workTime) {
            $this->totalWorkedAmount += $workTime->getDaytimeGain() + $workTime->getNightlyGain() + $workTime->getHolydayDaytimeGain() + $workTime->getHolydayNightlyGain();
        }
    }

    public function calculateTransportationSubsidy()
    {
        $employee = new Employee();
        $employee->setId($this->employee);
        $employee->query();
        
        $employeeType = new EmployeeType();
        $employeeType->setId($employee->getType());
        $employeeType->query();
        
        $numberOfDays = $this->calculateNumberOfDays();
        
        $this->transportationValue = round(($employeeType->getTransportSubsidy() / 30) * $numberOfDays);
    }

    public function calculateSecurity()
    {
        $employee = new Employee();
        $employee->setId($this->employee);
        $employee->query();
        
        $employeeType = new EmployeeType();
        $employeeType->setId($employee->getType());
        $employeeType->query();
        
        $this->securityValue = round(((($employeeType->getSalary() / 2) + $this->totalWorkedAmount) * $employeeType->getSocialSecurity()) / 100);
    }

    public function calculateTotalAmount()
    {
        $employee = new Employee();
        $employee->setId($this->employee);
        $employee->query();
        
        $employeeType = new EmployeeType();
        $employeeType->setId($employee->getType());
        $employeeType->query();
        
        $this->totalAmount = round((($this->salaryAmount + $this->totalWorkedAmount) - $this->securityValue) + $this->transportationValue);
    }

    public function calculateSalaryBase()
    {
        $employee = new Employee();
        $employee->setId($this->employee);
        $employee->query();
        
        $employeeType = new EmployeeType();
        $employeeType->setId($employee->getType());
        $employeeType->query();
        
        $this->salaryAmount = $employeeType->getSalary() / 2;
    }

    private function calculateNumberOfDays()
    {
        $numberOfDays = [];
        
        foreach ($this->workedTimes as $worktime) {
            if (! in_array($worktime->getDate(), $numberOfDays)) {
                $numberOfDays[] = $worktime->getDate();
            }
        }
        
        return count($numberOfDays);
    }

    public function getDetailsExtraHours()
    {
        $diurnalHours = 0;
        $nocturnalHours = 0;
        $holyDiurnalHours = 0;
        $holyNocturnalHours = 0;
        $diurnalMinutes = 0;
        $nocturnalMinutes = 0;
        $holyDiurnalMinutes = 0;
        $holyNocturnalMinutes = 0;
        
        foreach ($this->workedTimes as $workedTime) {
            
            $diurnalHours += $workedTime->getDaytimeHours();
            $nocturnalHours += $workedTime->getNightlyHours();
            $holyDiurnalHours += $workedTime->getHolydayDaytimeHours();
            $holyNocturnalHours += $workedTime->getHolydayNightlyHours();
            $diurnalMinutes += $workedTime->getDaytimeMinutes();
            $nocturnalMinutes += $workedTime->getNightlyMinutes();
            $holyDiurnalMinutes += $workedTime->getHolydayDaytimeMinutes();
            $holyNocturnalMinutes += $workedTime->getHolydayNightlyMinutes();
        }
        
        $diurnalHours += floor($diurnalMinutes / 60);
        $diurnalMinutes = $diurnalMinutes % 60;
        
        $nocturnalHours += floor($nocturnalMinutes / 60);
        $nocturnalMinutes = $nocturnalMinutes % 60;
        
        $holyDiurnalHours += floor($holyDiurnalMinutes / 60);
        $holyDiurnalMinutes = $holyDiurnalMinutes % 60;
        
        $holyNocturnalHours += floor($holyNocturnalMinutes / 60);
        $holyNocturnalMinutes = $holyNocturnalMinutes % 60;
        
        return [
            'diurnalTime' => $diurnalHours . ':' . $diurnalMinutes,
            'nocturnalTime' => $nocturnalHours . ':' . $nocturnalMinutes,
            'holyDiurnalTime' => $holyDiurnalHours . ':' . $holyDiurnalMinutes,
            'holyNocturnalTime' => $holyNocturnalHours . ':' . $holyNocturnalMinutes
        ];
    }

    public function getLoans()
    {
        $loans = \Drupal::service('employee_salary.loan')->listEmployeeLoans($this->employee);
        
        $discounts = [];
        
        foreach ($loans as $loan) {
            $discounts[$loan->getId()]['description'] = $loan->getDescription();
            $discounts[$loan->getId()]['value'] = $loan->calculatePay();
        }
        
        return $discounts;
    }

    /**
     *
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     *
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     *
     * @return mixed
     */
    public function getInitialDate()
    {
        return $this->initialDate;
    }

    /**
     *
     * @param mixed $initialDate
     */
    public function setInitialDate($initialDate)
    {
        $this->initialDate = $initialDate;
    }

    /**
     *
     * @return mixed
     */
    public function getFinalDate()
    {
        return $this->finalDate;
    }

    /**
     *
     * @param mixed $finalDate
     */
    public function setFinalDate($finalDate)
    {
        $this->finalDate = $finalDate;
    }

    /**
     *
     * @return number
     */
    public function getTotalWorkedAmount()
    {
        return $this->totalWorkedAmount;
    }

    /**
     *
     * @param number $totalWorkedAmount
     */
    public function setTotalWorkedAmount($totalWorkedAmount)
    {
        $this->totalWorkedAmount = $totalWorkedAmount;
    }

    /**
     *
     * @return number
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     *
     * @param number $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     *
     * @return number
     */
    public function getSecurityValue()
    {
        return $this->securityValue;
    }

    /**
     *
     * @param number $securityValue
     */
    public function setSecurityValue($securityValue)
    {
        $this->securityValue = $securityValue;
    }

    /**
     *
     * @return number
     */
    public function getTransportationValue()
    {
        return $this->transportationValue;
    }

    /**
     *
     * @param number $transportationValue
     */
    public function setTransportationValue($transportationValue)
    {
        $this->transportationValue = $transportationValue;
    }

    /**
     *
     * @return number
     */
    public function getSalaryAmount()
    {
        return $this->salaryAmount;
    }

    /**
     *
     * @param number $salaryAmount
     */
    public function setSalaryAmount($salaryAmount)
    {
        $this->salaryAmount = $salaryAmount;
    }
}


<?php
namespace Drupal\employee_salary\Model;

class Loan
{

    const PAY_FORM_MONTH = 30;

    const PAY_FORM_BIWEEKLY = 15;

    const TABLE = 'employee_salary_loan';

    const ALIAS = 'lo';

    private $id;

    private $employee;

    private $description;

    private $payForm;

    private $numberOfPays;

    private $totalAmount;

    private $currentAmount;

    private $fistPayDate;

    public function validate()
    {
        $fieldsFail = [];
        
        /*
         * TODO: Write validation error messages.
         * TODO: refine the validations
         */
        
        if (is_null($this->getEmployee()) || empty($this->getEmployee())) {
            array_push($fieldsFail, [
                'name' => 'Employee',
                'message' => 'Debe proporcionar un tipo'
            ]);
        }
        
        if (is_null($this->getDescription()) || empty($this->getDescription())) {
            array_push($fieldsFail, [
                'name' => 'loanDescription',
                'message' => 'Debe proporcionar una descripcion'
            ]);
        }
        
        if (is_null($this->getPayForm()) || empty($this->getPayForm())) {
            array_push($fieldsFail, [
                'name' => 'loanPayForm',
                'message' => 'Debe proporcionar una forma de pago'
            ]);
        }
        
        if (is_null($this->getNumberOfPays()) || empty($this->getNumberOfPays())) {
            array_push($fieldsFail, [
                'name' => 'loanNumberOfPays',
                'message' => 'Debe proporcionar un numero de pagos'
            ]);
        }
        
        if (is_null($this->getTotalAmount()) || empty($this->getTotalAmount())) {
            array_push($fieldsFail, [
                'name' => 'loanAmount',
                'message' => 'Debe proporcionar una cantidad'
            ]);
        }
        
        if (is_null($this->getFistPayDate()) || empty($this->getFistPayDate())) {
            array_push($fieldsFail, [
                'name' => 'loanDateOfFirstPay',
                'message' => 'Debe proporcionar el dia del primer pago'
            ]);
        }
        
        return $fieldsFail;
    }

    public function save()
    {
        
        /*
         * TODO: get Current User na save it
         */
        $database = \Drupal::database();
        return $database->insert(Loan::TABLE)
            ->fields([
            'employee' => $this->getEmployee(),
            'description' => $this->getDescription(),
            'payForm' => $this->getPayForm(),
            'numberOfPays' => $this->getNumberOfPays(),
            'totalAmount' => $this->getTotalAmount(),
            'currentAmount' => $this->getTotalAmount(),
            'firstPayDate' => $this->getFistPayDate(),
            'createdAt' => time(),
            'createdBy' => ''
        
        ])
            ->execute();
    }

    public static function listLoansDescriptions()
    {
        $database = \Drupal::database();
        $query = $database->select(Loan::TABLE);
        $query->addExpression('id', 'id');
        $query->addExpression("concat(id,' - ',description)");
        
        $loanList = $query->execute()->fetchAll(\PDO::FETCH_KEY_PAIR);
        
        return $loanList;
    }

    public function listLoans()
    {
        $database = \Drupal::database();
        $query = $database->select(Loan::TABLE, Loan::ALIAS);
        $query->fields(Loan::ALIAS, [
            'id',
            'employee',
            'description',
            'payForm',
            'numberOfPays',
            'totalAmount',
            'currentAmount',
            'firstPayDate'
        ]);
        return $query->execute()->fetchAll(\PDO::FETCH_OBJ);
    }

    public function query()
    {
        $database = \Drupal::database();
        $data = $database->select(Loan::TABLE, Loan::ALIAS)
            ->fields(Loan::ALIAS, [
            'id',
            'employee',
            'description',
            'payForm',
            'numberOfPays',
            'totalAmount',
            'currentAmount',
            'firstPayDate'
        ])
            ->condition('id', $this->id)
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
        
        if (! empty($data)) {
            
            $this->setEmployee($data[0]->employee);
            $this->setDescription($data[0]->description);
            $this->setPayForm($data[0]->payForm);
            $this->setNumberOfPays($data[0]->numberOfPays);
            $this->setTotalAmount($data[0]->totalAmount);
            $this->setCurrentAmount($data[0]->firstPayDate);
        }
    }

    public function update()
    {
        
        /*
         * TODO: get Current User na save it
         */
        $database = \Drupal::database();
        return $database->update(Loan::TABLE)
            ->fields([
            'employee' => $this->getEmployee(),
            'description' => $this->getDescription(),
            'payForm' => $this->getPayForm(),
            'numberOfPays' => $this->getNumberOfPays(),
            'totalAmount' => $this->getTotalAmount(),
            'currentAmount' => $this->getCurrentAmount(),
            'firstPayDate' => $this->getFirstPayDate()
        ])
            ->condition('id', $this->id)
            ->execute();
    }

    public function delete()
    {
        $database = \Drupal::database();
        return $database->delete(Loan::TABLE)
            ->condition('id', $this->id)
            ->execute();
    }

    public function getHeaders()
    {
        $headers = [
            'id' => 'Id',
            'employee' => 'Empleado',
            'description' => 'Descripción',
            'totalAmount' => 'Valor Total',
            'currentAmount' => 'Valor Actual',
            'edit' => '',
            'delete' => ''
        ];
        
        return $headers;
    }

    public function listEmployeeLoans($employee)
    {
        $database = \Drupal::database();
        $data = $database->select(Loan::TABLE, Loan::ALIAS)->fields(Loan::ALIAS, [
            'id',
            'employee',
            'description',
            'payForm',
            'numberOfPays',
            'totalAmount',
            'currentAmount',
            'firstPayDate'
        ]);
        
        $andCondition = $data->andConditionGroup();
        $andCondition->condition('employee', $employee);
        $andCondition->condition('firstPayDate', time(), '>');
        $andCondition->condition('currentAmount', 0, '>');
        
        $data->condition($andCondition);
        $result = $data->execute()->fetchAll(\PDO::FETCH_CLASS, 'Drupal\employee_salary\Model\Loan');
        
        return $result;
    }

    public function calculatePay()
    {
        if ($this->validateDateOfPay()) {
            if ($this->validateTypeOfPay()) {
                $value = $this->calculatePayValue();
                if($this->makePayment($value)){
                    $this->setCurrentAmount($this->getCurrentAmount() - $value);
                    $this->update();
                }
                return $value;
            }
        }
    }
    
    private function makePayment($value){
        $payment = \Drupal::service('employee_salary.payment');
        
        $payment->setAmount($value);
        $payment->setLoan($this->id);
        $payment->setType('discount');
        
        if($payment->save()){
            return true;            
        } else {
            return false;
        }
    }

    private function calculatePayValue()
    {
        $value = round($this->totalAmount / $this->numberOfPays);
        
        return $value;
    }

    private function validateDateOfPay()
    {
        $datePayInit = \DateTime::createFromFormat('Y-m-d', $this->getFirstPayDate());
        $datePayInit->setTime(0, 0);
        
        $date = new \DateTime();
        
        if ($date >= $datePayInit) {
            return true;
        } else {
            return false;
        }
    }

    private function validateTypeOfPay()
    {
        $payment = \Drupal::service('employee_salary.payment');
        
        $payments = $payment->getLastPaymentOfLoan($this->id);
        
        if (! empty($payments)) {
            
            $dateLastPay = new \DateTime();
            $dateLastPay->setTimestamp($payments[0]->createdAt);
            
            $date = new \DateTime();
            
            $dateDiff = $dateLastPay->diff($date);
            
            if ($dateDiff->days >= $this->payForm) {
                return true;
            } else {
                return false;
            }
        } else {
            $datePayInit = \DateTime::createFromFormat('Y-m-d', $this->getFirstPayDate());
            $datePayInit->setTime(0, 0);
            
            $date = new \DateTime();
            
            $dateDiff = $datePayInit->diff($date);
            
            if ($dateDiff->days >= $this->payForm) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     *
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     *
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     *
     * @return mixed
     */
    public function getPayForm()
    {
        return $this->payForm;
    }

    /**
     *
     * @param mixed $payForm
     */
    public function setPayForm($payForm)
    {
        $this->payForm = $payForm;
    }

    /**
     *
     * @return mixed
     */
    public function getNumberOfPays()
    {
        return $this->numberOfPays;
    }

    /**
     *
     * @param mixed $numberOfPays
     */
    public function setNumberOfPays($numberOfPays)
    {
        $this->numberOfPays = $numberOfPays;
    }

    /**
     *
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     *
     * @param mixed $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     *
     * @return mixed
     */
    public function getCurrentAmount()
    {
        return $this->currentAmount;
    }

    /**
     *
     * @param mixed $currentAmount
     */
    public function setCurrentAmount($currentAmount)
    {
        $this->currentAmount = $currentAmount;
    }

    /**
     *
     * @return mixed
     */
    public function getFirstPayDate()
    {
        return $this->firstPayDate;
    }

    /**
     *
     * @param mixed $fistPayDate
     */
    public function setFirstPayDate($firstPayDate)
    {
        $this->firstPayDate = $firstPayDate;
    }
}


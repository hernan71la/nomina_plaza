<?php
namespace Drupal\employee_salary\Model;

use Drupal\employee_salary\Util\Validator;

class Employee
{

    const TABLE = 'employee_salary_employee';

    const ALIAS = 'em';

    private $id;

    private $type;

    private $document;

    private $name;

    private $lastName;

    private $phone;

    private $email;

    private $address;

    public function validate()
    {
        $fieldsFail = [];
        
        /*
         * TODO: Write validation error messages.
         * TODO: refine the validations
         */
        
        if (is_null($this->getType()) || empty($this->getType())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeType',
                'message' => 'Debe proporcionar un tipo'
            ]);
        }
        
        if (is_null($this->getName()) || empty($this->getName())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeName',
                'message' => 'Debe proporcionar un nombre'
            ]);
        }
        
        if (is_null($this->getLastName()) || empty($this->getLastName())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeLastName',
                'message' => 'Debe proporcionar un apellido'
            ]);
        }
        
        if (is_null($this->getDocument()) || empty($this->getDocument())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeDocument',
                'message' => 'Debe proporcionar un documento'
            ]);
        } else {
            if (! Validator::validatePattern($this->getDocument(), Validator::DOCUMENT_PATTERN)) {
                array_push($fieldsFail, [
                    'name' => 'EmployeeDocument',
                    'message' => 'Debe proporcionar un documento valido'
                ]);
            }
        }
        
        if (is_null($this->getPhone()) || empty($this->getPhone())) {
            array_push($fieldsFail, [
                'name' => 'EmployeePhone',
                'message' => 'Debe proporcionar un telefono'
            ]);
        }
        
        if (is_null($this->getEmail()) || empty($this->getEmail())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeEmail',
                'message' => 'Debe proporcionar un correo'
            ]);
        }
        
        if (is_null($this->getAddress()) || empty($this->getAddress())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeAddress',
                'message' => 'Debe proporcionar una direccion'
            ]);
        }
        
        return $fieldsFail;
    }

    public function save()
    {
        
        /*
         * TODO: get Current User na save it
         */
        $database = \Drupal::database();
        return $database->insert(Employee::TABLE)
            ->fields([
            'employeeType' => $this->getType(),
            'document' => $this->getDocument(),
            'name' => $this->getName(),
            'lastName' => $this->getLastName(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'address' => $this->getAddress(),
            'createdAt' => time(),
            'createdBy' => ''
        
        ])
            ->execute();
    }

    public static function listEmployeeNames()
    {
        $database = \Drupal::database();
        $query = $database->select(Employee::TABLE);
        $query->addExpression("id", "id");
        $query->addExpression("concat(name, ' ', lastName)", "name");
        
        $actives = WorkTime::active();
        
        if (! empty($actives)) {
            $andCondition = $query->andConditionGroup();
            $andCondition->condition('id', $actives, 'NOT IN');
            $andCondition->isNull('retiredAt');
            $query->condition($andCondition);
        } else {
            $query->isNull('retiredAt');
        }
        
        $employeeList = $query->execute()->fetchAll(\PDO::FETCH_KEY_PAIR);
        
        return $employeeList;
    }
    
    public static function listAllEmployeeNames()
    {
        $database = \Drupal::database();
        $query = $database->select(Employee::TABLE);
        $query->addExpression("id", "id");
        $query->addExpression("concat(name, ' ', lastName)", "name");
        $query->isNull('retiredAt');
        
        $employeeList = $query->execute()->fetchAll(\PDO::FETCH_KEY_PAIR);
        
        return $employeeList;
    }

    public function listEmployees()
    {
        $database = \Drupal::database();
        $query = $database->select(Employee::TABLE, Employee::ALIAS);
        $query->fields(Employee::ALIAS, [
            'id',
            'employeeType',
            'document',
            'name',    
            'lastName',    
            'phone',    
            'email',    
            'address'
        ]);
        return $query->execute()->fetchAll(\PDO::FETCH_OBJ);
        
    }
    
    public function query()
    {
        $database = \Drupal::database();
        $data = $database->select(Employee::TABLE, Employee::ALIAS)
        ->fields(Employee::ALIAS, [
            'id',
            'employeeType',
            'document',
            'name',
            'lastName',
            'phone',
            'email',
            'address'
        ])
        ->condition('id', $this->id)
        ->execute()
        ->fetchAll(\PDO::FETCH_OBJ);
        
        if (! empty($data)) {
            
            $this->setType($data[0]->employeeType);
            $this->setDocument($data[0]->document);
            $this->setName($data[0]->name);
            $this->setLastName($data[0]->lastName);
            $this->setPhone($data[0]->phone);
            $this->setEmail($data[0]->email);
            $this->setAddress($data[0]->address);
            
        }
    }
    
    public function update()
    {
        
        /*
         * TODO: get Current User na save it
         */
        $database = \Drupal::database();
        return $database->update(Employee::TABLE)
        ->fields([
            'employeeType' => $this->getType(),
            'document' => $this->getDocument(),
            'name' => $this->getName(),
            'lastName' => $this->getLastName(),
            'phone' => $this->getPhone(),
            'email'=> $this->getEmail(),
            'address' => $this->getAddress()
        ])
        ->condition('id', $this->id)
        ->execute();
    }
    
    public function delete()
    {
        $database = \Drupal::database();
        return $database->delete(Employee::TABLE)
        ->condition('id', $this->id)
        ->execute();
    }

    public function getHeaders()
    {
        $headers = [
            'id' => 'Id',
            'name' => 'Nombre',
            'type' => 'Tipo',
            'phone' => 'Telefono',
            'edit' => '',
            'delete' => ''
        ];
        
        return $headers;
    }

    /**
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     *
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     *
     * @param mixed $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     *
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     *
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     *
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     *
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     *
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     *
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}


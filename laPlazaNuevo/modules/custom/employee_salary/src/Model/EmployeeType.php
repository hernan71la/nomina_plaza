<?php
namespace Drupal\employee_salary\Model;

class EmployeeType
{

    const TABLE = 'employee_salary_employee_type';

    const ALIAS = 'et';

    private $id;

    private $name;

    private $description;

    private $salary;

    private $hourValue;

    private $transportSubsidy;

    private $socialSecurity;

    private $numberWorkHours;

    public function validate()
    {
        $fieldsFail = [];
        
        /*
         * TODO: Write validation error messages.
         * TODO: refine the validations
         */
        
        if (is_null($this->getName()) || empty($this->getName())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeTypeName',
                'message' => 'Debe proporcionar un nombre'
            ]);
        }
        
        if (is_null($this->getDescription()) || empty($this->getDescription())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeTypeDescription',
                'message' => 'Debe proporcionar una descripcion'
            ]);
        }
        
        if (is_null($this->getSalary())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeTypeSalary',
                'message' => 'Debe proporcionar un salario'
            ]);
        }
        
        if (is_null($this->getHourValue()) || empty($this->getHourValue())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeTypeHourValue',
                'message' => 'Debe proporcionar un valor de hora'
            ]);
        }
        
        if (is_null($this->getTransportSubsidy()) || empty($this->getTransportSubsidy())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeTypeTramsportationSubsidy',
                'message' => 'Debe proporcionar un subsidio de transporte'
            ]);
        }
        
        if (is_null($this->getSocialSecurity()) || empty($this->getSocialSecurity())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeTypeSocialSecuriity',
                'message' => 'Debe proporcionar una seguridad social'
            ]);
        }
        
        if (is_null($this->getNumberWorkHours())) {
            array_push($fieldsFail, [
                'name' => 'EmployeeTypeNumberHours',
                'message' => 'Debe proporcionar un numero de horas'
            ]);
        }
        
        return $fieldsFail;
    }

    public function save()
    {
        
        /*
         * TODO: get Current User na save it
         */
        $database = \Drupal::database();
        return $database->insert(EmployeeType::TABLE)
            ->fields([
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'salary' => $this->getSalary(),
            'hourValue' => $this->getHourValue(),
            'transportationSubsidy' => $this->getTransportSubsidy(),
            'socialSecurity' => $this->getSocialSecurity(),
            'numberWorkHours' => $this->getNumberWorkHours(),
            'createdAt' => time(),
            'createdBy' => ''
        
        ])
            ->execute();
    }

    public function update()
    {
        
        /*
         * TODO: get Current User na save it
         */
        $database = \Drupal::database();
        return $database->update(EmployeeType::TABLE)
            ->fields([
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'salary' => $this->getSalary(),
            'hourValue' => $this->getHourValue(),
            'transportationSubsidy' => $this->getTransportSubsidy(),
            'socialSecurity' => $this->getSocialSecurity(),
            'numberWorkHours' => $this->getNumberWorkHours()
        ])
            ->condition('id', $this->id)
            ->execute();
    }

    public function query()
    {
        
        /*
         * TODO: get Current User na save it
         */
        $database = \Drupal::database();
        $data = $database->select(EmployeeType::TABLE, EmployeeType::ALIAS)
            ->fields(EmployeeType::ALIAS, [
            'name',
            'description',
            'salary',
            'hourValue',
            'transportationSubsidy',
            'socialSecurity',
            'numberWorkHours'
        ])
            ->condition('id', $this->id)
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
        
        if (! empty($data)) {
            $this->setName($data[0]->name);
            $this->setDescription($data[0]->description);
            $this->setSalary($data[0]->salary);
            $this->setHourValue($data[0]->hourValue);
            $this->setTransportSubsidy($data[0]->transportationSubsidy);
            $this->setSocialSecurity($data[0]->socialSecurity);
            $this->setNumberWorkHours($data[0]->numberWorkHours);
        }
    }

    public function delete()
    {
        $database = \Drupal::database();
        return $database->delete(EmployeeType::TABLE)
            ->condition('id', $this->id)
            ->execute();
    }

    public static function listEmployeeTypeName()
    {
        $database = \Drupal::database();
        
        $query = $database->select(EmployeeType::TABLE, EmployeeType::ALIAS);
        $query->fields(EmployeeType::ALIAS, [
            'id',
            'name'
        ]);
        $employeeTypesList = $query->execute()->fetchAll(\PDO::FETCH_KEY_PAIR);
        
        return $employeeTypesList;
    }

    public function listEmployType()
    {
        $database = \Drupal::database();
        
        $query = $database->select(EmployeeType::TABLE, EmployeeType::ALIAS);
        $query->fields(EmployeeType::ALIAS, [
            'id',
            'name',
            'description',
            'salary',
            'hourValue',
            'transportationSubsidy',
            'socialSecurity',
            'numberWorkHours'
        ]);
        $employeeTypesList = $query->execute()->fetchAll(\PDO::FETCH_OBJ);
        
        return $employeeTypesList;
    }

    public function getHeaders()
    {
        $headers = [
            'id' => 'Id',
            'name' => 'Nombre',
            'description' => 'Descripción',
            'edit' => '',
            'delete' => ''
        ];
        
        return $headers;
    }

    /**
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     *
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     *
     * @return mixed
     */
    public function getHourValue()
    {
        return $this->hourValue;
    }

    /**
     *
     * @return mixed
     */
    public function getTransportSubsidy()
    {
        return $this->transportSubsidy;
    }

    /**
     *
     * @return mixed
     */
    public function getSocialSecurity()
    {
        return $this->socialSecurity;
    }

    /**
     *
     * @return mixed
     */
    public function getNumberWorkHours()
    {
        return $this->numberWorkHours;
    }

    /**
     *
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     *
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     *
     * @param mixed $hourValue
     */
    public function setHourValue($hourValue)
    {
        $this->hourValue = $hourValue;
    }

    /**
     *
     * @param mixed $transportSubsidy
     */
    public function setTransportSubsidy($transportSubsidy)
    {
        $this->transportSubsidy = $transportSubsidy;
    }

    /**
     *
     * @param mixed $socialSecurity
     */
    public function setSocialSecurity($socialSecurity)
    {
        $this->socialSecurity = $socialSecurity;
    }

    /**
     *
     * @param mixed $numberWorkHours
     */
    public function setNumberWorkHours($numberWorkHours)
    {
        $this->numberWorkHours = $numberWorkHours;
    }
}


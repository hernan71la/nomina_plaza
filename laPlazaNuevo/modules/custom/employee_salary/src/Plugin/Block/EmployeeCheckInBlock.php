<?php
namespace Drupal\employee_salary\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal;

/**
 * Provides a 'EmployeeCheckInBlock' block.
 *
 * @Block(
 * id = "employee_salary_employee_check_in_block",
 * admin_label = @Translation("Employee Check In block"),
 * )
 */
class EmployeeCheckInBlock extends BlockBase
{

    /**
     *
     * {@inheritdoc}
     */
    public function build()
    {
        
        return Drupal::formBuilder()->getForm('Drupal\employee_salary\Form\EmployeeCheckInForm');
        
    }

}

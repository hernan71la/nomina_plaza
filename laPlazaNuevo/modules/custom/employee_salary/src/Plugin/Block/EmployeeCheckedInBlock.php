<?php
namespace Drupal\employee_salary\Plugin\Block;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal;
use Drupal\employee_salary\Model\WorkTime;
use Drupal\employee_salary\Model\Employee;

/**
 * Provides a 'EmployeeCheckedInBlock' block.
 *
 * @Block(
 * id = "employee_salary_employee_checked_in_block",
 * admin_label = @Translation("Employee Checked In block"),
 * )
 */
class EmployeeCheckedInBlock extends BlockBase implements ContainerFactoryPluginInterface
{

    /**
     *
     * @var \Drupal\employee_salary\Model\WorkTime
     */
    protected $workTime;
    
    /**
     * Drupal\employee_salary\Model\Employee
     *
     * @var object
     */
    protected $employee;

    /**
     *
     * {@inheritdoc}
     * @see \Drupal\Core\Block\BlockBase::__construct()
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, WorkTime $worktime, Employee $employee)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->workTime = $worktime;
        $this->employee = $employee;
    }

    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        
        return new static($configuration,$plugin_id,$plugin_definition,$container->get('employee_salary.work_time'), $container->get('employee_salary.employee'));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function build()
    {
        $header = [];
        $employeesCheckedIn = $this->workTime->getActiveWorkers();
        
        // The table description.
        $build['table'] = [
            '#type' => 'table',
            '#header' => $header,
            '#rows' => [],
            '#empty' => $this->t('There is no yet.')
        ];
        
        foreach ($employeesCheckedIn as $employeeChekedIn) {
            
            $this->employee->setId($employeeChekedIn->employee);
            $this->employee->query();
            
            $row['name']['data'] = $this->employee->getName() . ' ' . $this->employee->getLastName();
            
            $urlClose = Url::fromRoute('employee_salary.worktime_finish', [
                'id' => $employeeChekedIn->id
            ]);
            
            $row['close']['data'] = Link::fromTextAndUrl('Close', $urlClose);
            
            $build['table']['#rows'][$employeeChekedIn->id] = $row;
        }
        
        $build['pager'] = [
            '#type' => 'pager',
            '#quantity' => 10
        ];
        
        return $build;
        
    }

}
